# Pi2



#### 介绍
基于E4（Eclipse 4 RCP）平台实现的，局域网的文件传输的小工具。



#### 安装教程
请参考：[Pi2（派兔）开发文档](https://www.bbstone.cn/?p=438)

#### 使用说明

请参考：[Pi2（派兔）使用手册](https://www.bbstone.cn/?p=404)



#### 软件架构

##### RCP GUI设计

GUI是基于Eclipse 4 RCP进行设计和开发的。各个 组件之间的结构如下：

![输入图片说明](architect.png)


说明如下：
总的来说，Pi2的RCP可以分成三大部分，features，target platform和product，
Features
feature是插件的分组管理工具，而插件就是pi2的功能模块划分单元，feature分组主要有以下几个部分：
-  biz业务功能有关的插件集（feature），由两部分组成，一是、文件传输功能实现的插件，biz.server/biz.cmm/biz.client等；二是、biz中必不可少的日志插件。
-  这里是列表文本RCP使用到的第三方插件集（feature）。也由两部分组成，一是、使用了第三方实现的UI组件：status bar和preference；二是、官方插件仓库没有提供的一些插件，例如, netty, hutool, fast-md5,Jackson等第三方插件，详见pi2-lib项目。
-  UI有关的插件集（feature），同样可分成两部分，一是、专为Pi2开发的UI插件，ui.model/ui.service/ui.cmm，还有提供RCP应用模型的插件cn.bbstone.pisces2.ui插件；二是、运行RCP所需的Eclipse提供的UI基础插件。
-  这里是列表文本OS平台特有的插件集（features），通常这部分提供三类平台的特需插件，win32，macosx，linux。

Target Platform目标平台
目标平台可以简单理解为项目的插件依赖总集，而各个feature或插件，所用到的插件都必须是在这个目标平台里提供有才可以选，feature和插件项目选择不到目标平台没包括进来的插件。
目标平台创建插件总集的有以下几种方式：

- Software Site：提供p2的update site方式来下载插件，来组成插件总集，即目标平台。
- Directory：通过指定本机某个目录，把该目录的插件添加到目标平台来。
- Installation：未使用（TODO：待验证）
- Feature：通过feature方式指定插件集。
- Target File：通过其他的目标平台文件target 文件来指定插件集
- Maven：通过maven的坐标，指定插件集。


Product产品
product产品，是Eclipse rcp提供的一个功能，主要用于打包插件集来组成一个插件，比较常见的是有两种产品分类：
- 打包成Eclipse的一个插件，然后发布到eclipse的插件市场，可以在Eclipse Marketplace 下载安装。
- 打包成独立的可运行的GUI，例如，本项目Pi2，就是这种方式，打包成可以独立运行的桌面端应用。
除此之外，也可以根据具体的需要，创建不同的产品类型，比如，想要发布社区版的产品，或者发布商业版本的产品，来提供更有价值的功能，等等。



#### 参与贡献
