#!/bin/sh
cd contrib
#mvn install:install-file -Dfile=com.ibm.icu.base_58.2.0.v20170418-1837.jar -DgroupId=com.ibm.icu -DartifactId=com.ibm.icu.base -Dversion=58.2.0 -Dpackaging=jar
mvn install:install-file \
	-Dfile=au.com.cybersearch2.control_factory-1.2.0-SNAPSHOT.jar \
	-DgroupId=au.com.cybersearch2 \
	-DartifactId=au.com.cybersearch2.control_factory -Dversion=1.2.0-SNAPSHOT -Dpackaging=jar
mvn install:install-file \
	-Dfile=au.com.cybersearch2.statusbar-1.2.0-SNAPSHOT.jar \
	-DgroupId=au.com.cybersearch2 \
	-DartifactId=au.com.cybersearch2.statusbar -Dversion=1.2.0-SNAPSHOT -Dpackaging=jar
mvn install:install-file \
	-Dfile=com.opcoach.e4.preferences-1.3.1.jar \
	-DgroupId=com.opcoach.e4.preferences \
	-DartifactId=com.opcoach.e4.preferences -Dversion=1.3.1 -Dpackaging=jar
mvn install:install-file \
	-Dfile=com.opcoach.e4.preferences.mainmenu-1.3.1 \
	-DgroupId=com.opcoach.e4.preferences \
	-DartifactId=com.opcoach.e4.preferences.mainmenu -Dversion=1.3.1 -Dpackaging=jar