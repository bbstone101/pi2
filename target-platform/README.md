# target-platform
	target-platform是设置插件项目的依赖其他的插件的总集合，即，插件项目中依赖的所有其他插件，
	都必须在target-platform定义的集合内选择，在target-platform之外的插件不可见。

## 介绍

target-platform.target

主要有两个目录（Eclipse和pi2-lib-bundle）如下：

```
		<location path="/Applications/Eclipse.app/Contents/Eclipse" type="Directory"/>
		<location path="http://localhost:8080/eclipse-repo" type="UpdateSite"/>
```


### Eclipse目录

	Eclipse目录是指向安装Eclipse的路径，使用的Eclipse IDE 版本信息如下：
	
	
```
Eclipse IDE for RCP and RAP Developers (includes Incubating components)

Version: 2022-06 (4.24.0)
Build id: 20220609-1112
```

### 依赖的第三方的bundle

	在pi2/product/local-repo，如以下树结构所示：

	
```
local-repo
├── README.md
├── check.sh
├── contrib
│   ├── au.com.cybersearch2.control_factory-1.2.0.jar
│   ├── au.com.cybersearch2.statusbar-1.2.0.jar
│   ├── com.opcoach.e4.preferences-1.3.1.jar
│   └── com.opcoach.e4.preferences.mainmenu-1.3.1.jar
├── install-3rd-party-deps.sh
├── pom.xml
├── start.sh
├── stop.sh
└── target
    ├── repository
    └── tmp
    
```


首先执行 start.sh(需要Linux或Mac环境），配置和启动本地插件仓库，
	用来提供第三依赖，运行 check.sh	确认是否已经成功启动，访问 http://localhost:8080/eclipse-repo/，看到如下所示的
	p2 update site的内容
	
	
	
```
Directory: /eclipse-repo/
artifacts.jar 	2974 bytes 	Sep 25, 2022, 12:52:52 PM
category.xml 	1217 bytes 	Sep 25, 2022, 12:52:52 PM
content.jar 	12195 bytes 	Sep 25, 2022, 12:52:53 PM
plugins/ 	832 bytes 	Sep 25, 2022, 12:52:52 PM	
	
```


## 其他说明
