package cn.bbstone.pisces2.comm.cache;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.bbstone.pisces2.comm.Const;
import cn.bbstone.pisces2.comm.fli.FliIndex;
import cn.bbstone.pisces2.comm.fli.FliSavePoint;
import cn.bbstone.pisces2.util.FLIUtil;

/**
 * read fli.idx lines to cache
 * send file_req to sever from cache items, if cache empty, read next lines to cache

 * there should only 2 case to use this client cache:
 * 1) the fist time run client, no fli.idx, no fli.sp file, fileNo start from 1( of fli.idx lines)
 * 2) run client from save-point fli.sp, req file from fileNo in sp.fileNo, then will sequence read next start from this fileNo
 *
 *
 */
public class ClientFliIndexCache {
	private static Logger log = LoggerFactory.getLogger(ClientFliIndexCache.class);
	
    private static int cacheSize = 0;
    private static FliIndex[] fliIndices = null;
    private static AtomicInteger nextCachePos = new AtomicInteger(0); // start from 0
    // fli.idx current line number, start from 1, currentPos should be same as fileNo in fli.idx
    private static AtomicLong currentPos = new AtomicLong(0L);
    private static FliSavePoint fliSavePoint;
    private static String cacheFlag = null;

    // initial cache
    public static void init(String serverOrClient) {
    	resetCurrentPos();
        // read batch line to cache
        if (Const.SERVER.equals(serverOrClient)) {
            fliSavePoint = FLIUtil.readServerSavePoint();
        } else {
            fliSavePoint = FLIUtil.readClientSavePoint();
        }
        cacheFlag = serverOrClient;
        // set cache size
        if (fliSavePoint.getCount() > 50000) { // > 5w
            cacheSize = 5000;
        } else if (fliSavePoint.getCount() > 10000) { // > 1w
            cacheSize = 2000;
        } else if (fliSavePoint.getCount() > 1000) { // > 1k
            cacheSize = 200;
        } else {
            cacheSize = 100;
        }
        fliIndices = new FliIndex[cacheSize];
        // first time load cache
        updateCache(cacheFlag, 0L, cacheSize);
    }
    
    public static void resetCurrentPos() {
    	// from 0 to max(fileNo)
    	currentPos.set(0L);
    	// cycle-loop cache pos
    	nextCachePos.set(0);
    }

    /**
     *  CASE 1: fileNo from 1 to end
     * get next line from fli.idx file
     * first search cache items, if not found, load lines from fli.idx file
     *
     * @return
     */
    public synchronized static FliIndex next() {
        // end of fli.idx
        if (currentPos.get() >= fliSavePoint.getCount()) {
        	FliIndex idx = new FliIndex();
        	idx.setFileNo(-1L);
        	idx.setFileCat(Const.BFILE_CAT_NA);
        	idx.setRpath(Const.RPATH_END_OF_INDEX);
            return idx;
        }
        //
        FliIndex fliIndex = null;
        log.debug("--<>-start-<>-- currentPos: {}, nextCachePos: {}", currentPos.get(), nextCachePos.get());
        // first line or
        if (nextCachePos.get() == 0L) {
            // read batch line to cache
//            updateCache(currentPos, cacheSize);
            // return the currentPos item
            fliIndex = fliIndices[0]; // the first line item
            nextCachePos.incrementAndGet();//++;
            currentPos.incrementAndGet();//++;
            log.debug("--<>-0-<>-- currentPos: {}, nextCachePos: {}", currentPos.get(), nextCachePos.get());
            return fliIndex;
        }
        // the last item in cache
        if (nextCachePos.get() == (cacheSize - 1)) {
            // return the last item in cache
            fliIndex = fliIndices[nextCachePos.get()];
            // reset next cache pos to 0;
            nextCachePos.set(0);// = 0; // the next cache pos of  last item is start from 0 again.
            currentPos.incrementAndGet();//++;
            // read next batch line to cache
            updateCache(cacheFlag, currentPos.get(), cacheSize);
            log.debug("--<>-n-<>-- currentPos: {}, nextCachePos: {}", currentPos.get(), nextCachePos.get());
            return fliIndex;
        }
        // hit cache (line in cache)
        fliIndex = fliIndices[nextCachePos.get()];
        nextCachePos.incrementAndGet();//++;
        currentPos.incrementAndGet();//++;
        log.debug("--<>-x-<>-- currentPos: {}, nextCachePos: {}", currentPos.get(), nextCachePos.get());
        return fliIndex;
    }

    /**
     *
     * @param cacheFlag - S or C,
     * @param fileNoOffset - start fileNo to update cacheItem
     * @param cacheSize -
     */
    private synchronized static void updateCache(String cacheFlag, long fileNoOffset, int cacheSize) {
        log.debug("==<>==<> fileNoOffset: {}, cacheSize: {}", fileNoOffset, cacheSize);
        List<FliIndex> itemList = Const.SERVER.equals(cacheFlag)
                ? FLIUtil.readServerIndexLines(fileNoOffset, cacheSize)
                : FLIUtil.readClientIndexLines(fileNoOffset, cacheSize);
        FliIndex[] itemArray = itemList.stream().toArray(FliIndex[]::new);
        if (fliSavePoint.getCount() - fileNoOffset > cacheSize) {
            System.arraycopy(itemArray, 0, fliIndices, 0, cacheSize);
        } else if (fliSavePoint.getCount() - fileNoOffset < cacheSize) {
            // the last cache may < cacheSize
            clearCache();
            System.arraycopy(itemArray, 0, fliIndices, 0, (int) (fliSavePoint.getCount() - fileNoOffset));
        } // eles if (currentPos + cacheSize == fliSavePoint.getCount()), reach the end of fli.idx, no item to update cache
    }

//    public static long getCurrentPos() {
//        return currentPos;
//    }

//    public static int getNextCachePos() {
//        return nextCachePos;
//    }

    /**
     * CASE 2: fileNo start from sp.fileNo(>1)
     *
     * read from client cache, client cache will not update currentPos & nextCachePos,
     * only be updated by invoke FliIndexCache.next() method
     *
     * @param fileNo - should > 0
     * @return
     */
    public synchronized static FliIndex getClientIndex(long fileNo) {
        if (fileNo == 0) {
        	FliIndex idx = new FliIndex();
        	idx.setFileCat(Const.BFILE_CAT_FILE);
        	idx.setRpath(FLIUtil.FLI_IDX);
            return idx;
//            return FliIndex.builder()
//                    .fileCat(Const.BFILE_CAT_FILE)
//                    .rpath(FLIUtil.FLI_IDX)
//                    .build();
        }
//        if (fileNo <= 0) {
//            throw new RuntimeException("fileNo here should be > 0.");
//        }
        if (first() != null && last() != null) {
        	log.debug("param.fileNo: {},cache first().fileNo: {}, last().fileNo: {}", fileNo, first().getFileNo(), last().getFileNo());
        }
        FliIndex fliIndex = null;
        // fileNo item hit cache
        if (fileNo >= first().getFileNo() && fileNo <= last().getFileNo()) {
            fliIndex = readIndexFromClientCache(fileNo);
            return fliIndex;
        } else { // fileNo item not hit cache
            // update cache items when fileNo > last fileNo in cache
            if (fileNo > last().getFileNo()) { // only fileNo > last fileNo in cache update, fileNo < first fileNo in cache ignore.
                currentPos.set(fileNo);// = fileNo; // will skip currentPos items to update cache
                updateCache(cacheFlag, fileNo -1, cacheSize);
                log.debug("after cache update: first.fileNo: {}, last.fileNo: {}", first().getFileNo(), last().getFileNo());
                fliIndex = readIndexFromClientCache(fileNo);
                if (fileNo != fliIndex.getFileNo()) {
                    log.warn("read server index by fileNo: {} after cache update fail, but read unexpected fliIndex with fileNo: {}", fileNo, fliIndex.getFileNo());
                    // TODO throw exception, and handler exception in upper method for clean up cache
                    throw new RuntimeException(String.format("read server index by fileNo: %d after cache update fail, but read unexpected fliIndex with fileNo: %d", fileNo, fliIndex.getFileNo()));
                }
            }
        }
/*
        // update cache currentPos = fileNo
        currentPos.set(fileNo);// = fileNo; // will skip currentPos items to update cache
        updateCache(cacheFlag, fileNo -1, cacheSize);
        log.debug("after cache update: first.fileNo: {}, last.fileNo: {}", first().getFileNo(), last().getFileNo());
        FliIndex fliIndex = null;
        fliIndex = readIndexFromClientCache(fileNo);
*/

        return fliIndex;
    }

    private static FliIndex readIndexFromClientCache(long fileNo) {
        int posx = (int) (fileNo - first().getFileNo());
        log.debug("FliIndex Item hit cache. posx: {}", posx);
        FliIndex fliIndex = fliIndices[posx];
        nextCachePos.set(posx + 1);// = posx + 1;
        currentPos.set(fileNo);// = fileNo;
        return fliIndex;
    }

    private static FliIndex readIndexFromClientCacheNotUpdatePos(long fileNo) {
        int posx = (int) (fileNo - first().getFileNo());
        log.debug("FliIndex Item hit cache. posx: {}", posx);
        FliIndex fliIndex = fliIndices[posx];
        return fliIndex;
    }

    public static FliIndex getClientIndexNotUpdatePos(long fileNo) {
        if (fileNo == 0) {
        	FliIndex idx = new FliIndex();
        	idx.setFileCat(Const.BFILE_CAT_FILE);
        	idx.setRpath(FLIUtil.FLI_IDX);
        	return idx;
//            return FliIndex.builder()
//                    .fileCat(Const.BFILE_CAT_FILE)
//                    .rpath(FLIUtil.FLI_IDX)
//                    .build();
        }
//        log.debug("param.fileNo: {},cache first().fileNo: {}, last().fileNo: {}", fileNo, first().getFileNo(), last().getFileNo());
        FliIndex fliIndex = null;
        // fileNo item hit cache
        if (fileNo >= first().getFileNo() && fileNo <= last().getFileNo()) {
            fliIndex = readIndexFromClientCacheNotUpdatePos(fileNo);
            return fliIndex;
        } else { // fileNo item not hit cache, read line from fli.idx directly
            fliIndex = FLIUtil.readClientIndexLine(fileNo);
        }
        return fliIndex;
    }

    private static void clearCache() {
        Arrays.fill(fliIndices, null);
    }

    public static FliIndex first() {
        return fliIndices[0];
    }

    /**
     * 
     *  NOTICE server update cache will skip D(Directory) line
     *  
     * @param fileNo
     * @return
     */
    public static FliIndex last() {
//    	int lastIndex = 0;
//    	if (fileNo > 0 && fileNo < fliSavePoint.getCount()) {
//    		return fliIndices[fileNo ++];
//    	}
//        int lastIndex = (int) ((fliSavePoint.getCount() - currentPos) < cacheSize ? (fliSavePoint.getCount() % cacheSize - 1) : (cacheSize - 1));
        int lastIndex = (int) ((fliSavePoint.getCount() - currentPos.get()) < cacheSize ? (fliSavePoint.getCount() - currentPos.get()) : (cacheSize - 1));
        // directory read last index(no set currentPos yet)
//        if (currentPos.get() == 0) lastIndex --;
        return fliIndices[lastIndex];
    }

    public static long getMaxFileNo() {
        return fliSavePoint.getCount();
    }

}
