package cn.bbstone.pisces2.comm.cache;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.bbstone.pisces2.comm.Const;
import cn.bbstone.pisces2.comm.fli.FliIndex;
import cn.bbstone.pisces2.comm.fli.FliSavePoint;
import cn.bbstone.pisces2.util.FLIUtil;

/**
 * read fli.idx lines to cache
 * send file_req to sever from cache items, if cache empty, read next lines to cache
 *
 * TODO separated cache to server and client independent cache
 *
 *
 */
public class ServerFliIndexCache {
	private static Logger log = LoggerFactory.getLogger(ServerFliIndexCache.class);
	
    private static int cacheSize = 0;
    private static FliIndex[] fliIndices = null;
    private static int nextCachePos = 0; // start from 0
    // fli.idx current line number, start from 1, currentPos should be same as fileNo in fli.idx
    private static long currentPos = 1L;
    private static FliSavePoint fliSavePoint;
    private static String cacheFlag = null;

    // initial cache
    public static void init(String serverOrClient) {
    	resetCurrentPos();
        // read batch line to cache
        if (Const.SERVER.equals(serverOrClient)) {
            fliSavePoint = FLIUtil.readServerSavePoint();
        } else {
            fliSavePoint = FLIUtil.readClientSavePoint();
        }
        cacheFlag = serverOrClient;
        // set cache size
        if (fliSavePoint.getCount() > 50000) { // > 5w
            cacheSize = 5000;
        } else if (fliSavePoint.getCount() > 10000) { // > 1w
            cacheSize = 2000;
        } else if (fliSavePoint.getCount() > 1000) { // > 1k
            cacheSize = 200;
        } else {
            cacheSize = 100;
        }
        fliIndices = new FliIndex[cacheSize];
        // first time load cache
        updateCache(cacheFlag, 0L, cacheSize);
    }

    public static void resetCurrentPos() {
    	// from 1 to max(fileNo)
    	currentPos = 1L;
    	// cycle-loop cache pos
    	nextCachePos = 0;
    }

    /**
     *
     * @param cacheFlag - S or C,
     * @param fileNoOffset - start fileNo to update cacheItem
     * @param cacheSize -
     */
    private static void updateCache(String cacheFlag, long fileNoOffset, int cacheSize) {
        List<FliIndex> itemList = Const.SERVER.equals(cacheFlag)
                ? FLIUtil.readServerIndexLines(fileNoOffset, cacheSize)
                : FLIUtil.readClientIndexLines(fileNoOffset, cacheSize);
        FliIndex[] itemArray = itemList.stream().toArray(FliIndex[]::new);
        if (fliSavePoint.getCount() - fileNoOffset > cacheSize) {
            System.arraycopy(itemArray, 0, fliIndices, 0, cacheSize);
        } else if (fliSavePoint.getCount() - fileNoOffset < cacheSize) {
            // the last cache may < cacheSize
            clearCache();
            System.arraycopy(itemArray, 0, fliIndices, 0, (int) (fliSavePoint.getCount() - fileNoOffset));
        } // eles if (currentPos + cacheSize == fliSavePoint.getCount()), reach the end of fli.idx, no item to update cache
    }

    public static long getCurrentPos() {
        return currentPos;
    }

    public static int getNextCachePos() {
        return nextCachePos;
    }

    /**
     * mostly client will read fli.idx line from 1 to end,
     * <p>
     * but server will getIndex by fileNo(which transfer from client), so there is not cache update for cacheFlag = server,
     * so, need to update cache when fileNo > last().getFileNo() when cacheFlag is server
     *
     * @param fileNo
     * @return
     */
    public synchronized static FliIndex getServerIndex(long fileNo) {
        // fileNo = 0, means fli.idx file
        if (fileNo == 0) {
        	FliIndex idx = new FliIndex();
        	idx.setFileCat(Const.BFILE_CAT_FILE);
        	idx.setRpath(FLIUtil.FLI_IDX);
        	return idx;
//            return FliIndex.builder()
//                    .fileCat(Const.BFILE_CAT_FILE)
//                    .rpath(FLIUtil.FLI_IDX)
//                    .build();
        }
        // fileNo not hit cache
        if (fileNo < first().getFileNo() || fileNo > last().getFileNo()) {
            currentPos = fileNo; // will skip currentPos items to update cache
            updateCache(cacheFlag, fileNo -1, cacheSize);
        }
        // read fli.idx line from cache
        FliIndex fliIndex = readIndexFromServerCache(fileNo);
        // validate return value whether correct
        if (fileNo != fliIndex.getFileNo()) {
            log.warn("read server index by fileNo: {} after cache update fail, but read unexpected fliIndex with fileNo: {}", fileNo, fliIndex.getFileNo());
            // TODO throw exception, and handler exception in upper method for clean up cache
            throw new RuntimeException(String.format("read server index by fileNo: %d after cache update fail, but read unexpected fliIndex with fileNo: %d", fileNo, fliIndex.getFileNo()));
        }
        return fliIndex;
    }

    /**
     * after read index from cache, will update currentPos & nextCachePos
     * every time cache update, the first().fileNo will be not start 1 from mod cacheSize
     *
     *
     * @param fileNo
     * @return
     */
    private static FliIndex readIndexFromServerCache(long fileNo) {
        // array index start from 0, fileNo start from 1, convert fileNo to array index
        // TODO check fileNo between first().fileNo and last().fileNo
        int posx = (int) (fileNo - first().getFileNo());
        log.debug("FliIndex Item hit cache. posx: {}", posx);
        FliIndex fliIndex = fliIndices[posx];
        nextCachePos = posx + 1;
        currentPos = fileNo;
        return fliIndex;
    }

//    private static FliIndex readIndexFromFile(long fileNo) {
//        FliIndex fliIndex = null;
//        // read FliIndex from fli.idx
//        String indexPath = Const.SERVER.equals(cacheFlag) ? FLIUtil.getServerIndexPath() : FLIUtil.getClientIndexPath();
//        try (Stream<String> stream = Files.lines(Paths.get(indexPath))) {
//            // e.g.  30,
//            String filter = fileNo + Const.FLI_FIELD_SEPARATOR;
//            List<String> stringLines = null;
//            stringLines = stream.filter(e -> e.startsWith(filter)).collect(Collectors.toList());
//            if (stringLines == null) {
//                throw new RuntimeException(String.format("fileNo line not exists in fli.idx(%s) file.", indexPath));
//            }
//            // When fli.idx rpath field contains the separator, will return multiple lines with same fileNo
//            if (stringLines.size() != 1) {
//                throw new RuntimeException(String.format("fileNo has multiple lines in fli.idx(%s) file.", indexPath));
//            }
//            fliIndex = FLIUtil.convertToFliIndex(stringLines.get(0));
//        } catch (IOException e) {
//            log.error("directly read fileNo index from fli.idx file error. fileNo: ", fileNo);
//        }
//        return fliIndex;
//    }

    private static void clearCache() {
        Arrays.fill(fliIndices, null);
    }

    public static FliIndex first() {
        return fliIndices[0];
    }

    // NOTICE server update cache will skip D line
    public static FliIndex last() {
        int lastIndex = (int) ((fliSavePoint.getCount() - currentPos) < cacheSize ? (fliSavePoint.getCount() - currentPos) : (cacheSize - 1));
        return fliIndices[lastIndex];
    }

    public static long getMaxFileNo() {
        return fliSavePoint.getCount();
    }

}
