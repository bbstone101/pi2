package cn.bbstone.pisces2.comm.fli;

public class FliSavePoint {
    private String checksum; // fli.idx checksum
    private long count; // total lines
    private long fileNo; // current handler line
    private long updateTime; // last updateTs
    
	public String getChecksum() {
		return checksum;
	}
	public void setChecksum(String checksum) {
		this.checksum = checksum;
	}
	public long getCount() {
		return count;
	}
	public void setCount(long count) {
		this.count = count;
	}
	public long getFileNo() {
		return fileNo;
	}
	public void setFileNo(long fileNo) {
		this.fileNo = fileNo;
	}
	public long getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(long updateTime) {
		this.updateTime = updateTime;
	}
    
    
}
