package cn.bbstone.pisces2.comm.fli;

import cn.bbstone.pisces2.util.BFileUtil;

public class FliIndex {
    // D-directory, F-file
    private String fileCat;
    // relative path
    private String rpath;
    // line number in fli.idx, start from 1
    private long fileNo;

    /**
     * NOTICE: when used FliIndex.getRpath4Client() will return the rpath will be escaped, not exactly same as rpath in fli.idx
     * @return
     */
    public String getRpath4Client() {
        return BFileUtil.escapePath(this.rpath);
    }

	public String getFileCat() {
		return fileCat;
	}

	public void setFileCat(String fileCat) {
		this.fileCat = fileCat;
	}

	public String getRpath() {
		return rpath;
	}

	public void setRpath(String rpath) {
		this.rpath = rpath;
	}

	public long getFileNo() {
		return fileNo;
	}

	public void setFileNo(long fileNo) {
		this.fileNo = fileNo;
	}
    
}
