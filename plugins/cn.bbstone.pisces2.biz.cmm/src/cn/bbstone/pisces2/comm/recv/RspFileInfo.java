package cn.bbstone.pisces2.comm.recv;

public class RspFileInfo {
	private Byte msgType; // 0x05
	private String msgId; // UUID
	private long fileNo; // item line no. in fli.idx
	private long fileSize; // fileNo file's fileSize(unit: Byte)
	private String checksum; // file checksum
	protected int chunks; // 文件总 chunk 数

	protected long reqTs;
	
	public long getReqTs() {
		return reqTs;
	}

	public void setReqTs(long reqTs) {
		this.reqTs = reqTs;
	}

	public int getChunks() {
		return chunks;
	}

	public void setChunks(int chunks) {
		this.chunks = chunks;
	}

	public Byte getMsgType() {
		return msgType;
	}

	public void setMsgType(Byte msgType) {
		this.msgType = msgType;
	}

	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	public long getFileNo() {
		return fileNo;
	}

	public void setFileNo(long fileNo) {
		this.fileNo = fileNo;
	}

	public long getFileSize() {
		return fileSize;
	}

	public void setFileSize(long fileSize) {
		this.fileSize = fileSize;
	}

	public String getChecksum() {
		return checksum;
	}

	public void setChecksum(String checksum) {
		this.checksum = checksum;
	}

}
