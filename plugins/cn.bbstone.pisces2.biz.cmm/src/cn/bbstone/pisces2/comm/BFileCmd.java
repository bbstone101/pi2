package cn.bbstone.pisces2.comm;

public interface BFileCmd {

//    /** client ack server's response **/
//    public static final int RSP_ACK = 3;
//
//    // server register request command
//    /** client send file download request */
    public static final String REQ_FILE = "REQ_FILE";
    public static final String REQ_LIST = "REQ_LIST";
//
//    // client register rsp commands
//    /** server response file bytes(with BFileRsp header info) */
//    public static final String RSP_FILE = "RSP_FILE";
//    public static final String RSP_DIR = "RSP_DIR";
//    public static final String RSP_LIST = "RSP_LIST";

    /** client ack server's response **/
    public static final int RSP_ACK = 3;

    // ----------------------------------------------------------
    // server register request command
    // req fli.idx
    public static final Byte REQ_LIST_INFO = 0x01;
    public static final Byte REQ_LIST_DATA = 0x11;

    /** client send file download request */
    public static final Byte REQ_FILE_INFO = 0x02;

    public static final Byte REQ_FILE_DATA = 0x07;


    // 0xFF- 1111 1111, the max Byte is: 0111 1111-0x6F, the highest 0 is sign
    // Byte[-128,127)
    public static final Byte REQ_LIST_INFO_ACK = 0x61;
    public static final Byte REQ_FILE_INFO_ACK = 0x62;
    public static final Byte REQ_FILE_INFO_SKIP_ACK = 0x65;
    public static final Byte REQ_FILE_DATA_ACK = 0x63;
    public static final Byte REQ_LIST_DATA_ACK = 0x64;

    // client register rsp commands
    public static final Byte RSP_LIST_INFO = 0x03;
    public static final Byte RSP_LIST_DATA = 0x04;

    /** server response file bytes(with BFileRsp header info) */
    public static final Byte RSP_FILE_INFO = 0x05;
    public static final Byte RSP_FILE_DATA = 0x06;

}
