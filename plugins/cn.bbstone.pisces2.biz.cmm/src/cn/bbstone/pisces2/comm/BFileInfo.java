package cn.bbstone.pisces2.comm;

import java.util.Objects;

public class BFileInfo {
    //
    private String filepath;

    // D - directory, F-file
    private String fileCat;

    // file fingerprint
    private String checksum;
    // bytes
    private long fileSize;

    public BFileInfo() {
    }

    public BFileInfo(String filepath, String fileCat, String checksum, long fileSize) {
        this.filepath = filepath;
        this.fileCat = fileCat;
        this.checksum = checksum;
        this.fileSize = fileSize;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BFileInfo)) return false;
        BFileInfo fileInfo = (BFileInfo) o;
        return fileSize == fileInfo.fileSize &&
                filepath.equals(fileInfo.filepath) &&
                fileCat.equals(fileInfo.fileCat) &&
                checksum.equals(fileInfo.checksum);
    }

    @Override
    public int hashCode() {
        return 17 + Objects.hash(filepath, fileCat, checksum, fileSize);
    }

	public String getFilepath() {
		return filepath;
	}

	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}

	public String getFileCat() {
		return fileCat;
	}

	public void setFileCat(String fileCat) {
		this.fileCat = fileCat;
	}

	public String getChecksum() {
		return checksum;
	}

	public void setChecksum(String checksum) {
		this.checksum = checksum;
	}

	public long getFileSize() {
		return fileSize;
	}

	public void setFileSize(long fileSize) {
		this.fileSize = fileSize;
	}

//    @Override
//    public String toString() {
//        return JSON.toJSONString(this);
//    }
    
}
