package cn.bbstone.pisces2.comm;

import io.netty.util.CharsetUtil;

public class Const {

    public static final String PATH_SPECIAL_CHAR = "[:*?|<>\"]";
    public static final String PATH_SPECIAL_CHAR_REPLACER = "_";

    // ---------- protocol ----------
    public static final int HEAD_LEN_REQ_FILE = 58;
    public static final int HEAD_LEN_REQ_INFO = 58;
    public static final int HEAD_LEN_REQ_INFO_ACK = 58;
    public static final int HEAD_LEN_REQ_DATA = 82;
    public static final int HEAD_LEN_REQ_DATA_ACK = 82;

    public static final int HEAD_LEN_RSP_INFO = 114; // not used
    public static final int HEAD_LEN_RSP_DATA = 108;

    // ---------- protocol ----------
    public static final String EMPTY_STR = "";
    public static final String COMMA_STR = ",";
    public static final String FLI_FIELD_SEPARATOR = "-|:|-";


    public static final String magic = "BBSTONE";
    public static final int magicLen = magic.getBytes(CharsetUtil.UTF_8).length;

    // cannot used ":-)" as delimiter, when send video will cause lost bytes
    public static final String bfile_info_prefix = "__10BBSTONE_BFILE_START01__";
    public static final int bfile_info_prefix_len = bfile_info_prefix.getBytes(CharsetUtil.UTF_8).length;;

    public static final String delimiter = "__10BBSTONE_BFILE_END01__";

    public static final String RPATH_END_OF_INDEX = "RPATH_END_OF_INDEX";

    /** directory */
    public static final String BFILE_CAT_DIR = "D";
    /** file */
    public static final String BFILE_CAT_FILE = "F";
    /** Symbolic Link */
    public static final String BFILE_CAT_SL = "SL";
    public static final String BFILE_CAT_NA = "NA";

    public static final String SERVER = "S";
    public static final String CLIENT = "C";


    public static final int ACK_OK = 0;
    public static final int ACK_FAIL = 1;

    /** default chunk size is 8k */
    public static final int DEFAULT_CHUNK_SIZE = 8 * 1024 * 1024; // 8k -> 8M

    /** default client receive buffer size: 32M */
    public static final int DEFAULT_CLIENT_RECV_BUFFER_SIZE = 32 * 1024 * 1024;

    /** regex in String.replaceAll() method */
    public static final String WIN_FILE_SEPARATOR = "\\\\";
    public static final String NIX_FILE_SEPARATOR = "\\/";

    public static final String WIN_FILE_SEPARATOR_RPATH = "\\";
    public static final String NIX_FILE_SEPARATOR_RPATH = "/";

    public static final String TYPE_INTEGER = Integer.class.getSimpleName(); // return Integer
    public static final String TYPE_LONG = Long.class.getSimpleName(); // return Long
    public static final String TYPE_STRING = String.class.getSimpleName(); // return String







}
