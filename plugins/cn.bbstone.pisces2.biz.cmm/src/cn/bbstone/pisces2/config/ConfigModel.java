package cn.bbstone.pisces2.config;

import org.eclipse.core.runtime.Platform;

public class ConfigModel {

//	private static final boolean win = Platform.getOS().equals(Platform.OS_WIN32);
//	private static final boolean mac = Platform.getOS().equals(Platform.OS_MACOSX);
//	private static final boolean lin = Platform.getOS().equals(Platform.OS_LINUX);

	public static final String EMPTY_STR = "";
	
	public static final String SERVER_FLI_DIR_KEY = "fli.server.dir";
	public static final String CLIENT_FLI_DIR_KEY = "fli.client.dir";
	public static final String FLI_FILTER_KEY = "fli.filter";

	public static final String SERVER_PORT_KEY = "server.port";
	public static final String SERVER_HOST_KEY = "server.host";

	public static final String SERVER_DIR_MAC_KEY = "server.dir.mac"; // macos
	public static final String SERVER_DIR_NIX_KEY = "server.dir.nix";
	public static final String SERVER_DIR_WIN_KEY = "server.dir.win";

	public static final String CLIENT_DIR_MAC_KEY = "client.dir.mac"; // macos
	public static final String CLIENT_DIR_NIX_KEY = "client.dir.nix";
	public static final String CLIENT_DIR_WIN_KEY = "client.dir.win";

	public static final String CLIENT_FILE_OVERWRITE = "client.file.overwrite";
	public static final String CLIENT_CONN_NUM_KEY = "client.core.conn.num";
	public static final String CLIENT_RECV_BUFFER_KEY = "client.core.recv.buffer";
	// public static final String FLI_CACHE_SIZE = "fli.cache.size";
	public static final String FILE_TEMP_POSTFIX_KEY = "file.temp.postfix";

	// --- common
	protected String filter;

	// --- server fields
	protected String serverHost;
	protected int serverPort;

	protected String serverRoot;
	protected String serverRootMac; // macosx
	protected String serverRootNix; // linux
	protected String serverRootWin; // window

	// client fields
	protected String clientRoot;
	protected String clientRootMac; // macos
	protected String clientRootNix; // linux
	protected String clientRootWin; // window

	protected String postfix;
	protected boolean overwrite;
	protected int connNum;
	protected String recvBuffer;

	public String getFilter() {
		return filter;
	}

	public void setFilter(String filter) {
		this.filter = filter;
	}

	public String getServerHost() {
		return serverHost;
	}

	public void setServerHost(String serverHost) {
		this.serverHost = serverHost;
	}

	public int getServerPort() {
		return serverPort;
	}

	public void setServerPort(int serverPort) {
		this.serverPort = serverPort;
	}

	public String getServerRootMac() {
		return serverRootMac;
	}

	public void setServerRootMac(String serverRootMac) {
		this.serverRootMac = serverRootMac;
	}

	public String getClientRootMac() {
		return clientRootMac;
	}

	public void setClientRootMac(String clientRootMac) {
		this.clientRootMac = clientRootMac;
	}

	public String getServerRootNix() {
		return serverRootNix;
	}

	public void setServerRootNix(String serverRootNix) {
		this.serverRootNix = serverRootNix;
	}

	public String getServerRootWin() {
		return serverRootWin;
	}

	public void setServerRootWin(String serverRootWin) {
		this.serverRootWin = serverRootWin;
	}

	public String getClientRootNix() {
		return clientRootNix;
	}

	public void setClientRootNix(String clientRootNix) {
		this.clientRootNix = clientRootNix;
	}

	public String getClientRootWin() {
		return clientRootWin;
	}

	public void setClientRootWin(String clientRootWin) {
		this.clientRootWin = clientRootWin;
	}

	public String getPostfix() {
		return postfix;
	}

	public void setPostfix(String postfix) {
		this.postfix = postfix;
	}

	public boolean isOverwrite() {
		return overwrite;
	}

	public void setOverwrite(boolean overwrite) {
		this.overwrite = overwrite;
	}

	public int getConnNum() {
		return connNum;
	}

	public void setConnNum(int connNum) {
		this.connNum = connNum;
	}

	public String getRecvBuffer() {
		return recvBuffer;
	}

	public void setRecvBuffer(String recvBuffer) {
		this.recvBuffer = recvBuffer;
	}

	public String getServerRoot() {
		/**
		 * once preference set serverRoot, preference serverRoot value will store on this.serverRoot
		 */
		if (this.serverRoot != null && this.serverRoot.trim().length() != 0) {
			return this.serverRoot;
		}
		// get default server root path
		String os = Platform.getOS();
		switch (os) {
			case Platform.OS_MACOSX:
				return getServerRootMac();
			case Platform.OS_LINUX:
				return getServerRootNix();
			case Platform.OS_WIN32:
				return getServerRootWin();
			default:
				break;
		}
		return EMPTY_STR;
	}

	public String getClientRoot() {
		/**
		 * once preference set clientRoot, preference clientRoot value will store on this.clientRoot
		 */
		if (this.clientRoot != null && this.clientRoot.trim().length() != 0) {
			return this.clientRoot;
		}
		// get default client root path
		String os = Platform.getOS();
		switch (os) {
			case Platform.OS_MACOSX:
				return getClientRootMac();
			case Platform.OS_LINUX:
				return getClientRootNix();
			case Platform.OS_WIN32:
				return getClientRootWin();
			default:
				break;
		}
		return EMPTY_STR;
	}

	public void setServerRoot(String serverRoot) {
		this.serverRoot = serverRoot;
	}

	public void setClientRoot(String clientRoot) {
		this.clientRoot = clientRoot;
	}

}
