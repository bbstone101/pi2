package cn.bbstone.pisces2.config;

public interface Config {

	public ConfigModel load(String serverOrClient, ClassLoader classLoader);
	public ConfigModel getModel();
	public void store(ConfigModel config);

}
