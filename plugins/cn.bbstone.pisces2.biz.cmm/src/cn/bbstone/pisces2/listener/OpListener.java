package cn.bbstone.pisces2.listener;

/**
 * Operation Listener
 */
public interface OpListener {
    void operate(OpEvent opEvent);
}
