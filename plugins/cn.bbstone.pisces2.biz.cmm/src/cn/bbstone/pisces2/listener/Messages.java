package cn.bbstone.pisces2.listener;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = Messages.class.getPackageName() + ".messages"; //$NON-NLS-1$
	public static String OpEnum_client_biz_meta_created;
	public static String OpEnum_client_biz_meta_loaded;
	public static String OpEnum_done_loading_file_list;
	public static String OpEnum_done_scan_file_list;
	public static String OpEnum_loading_file_list;
	public static String OpEnum_Other;
	public static String OpEnum_s1_initial;
	public static String OpEnum_s101_client_shutdown;
	public static String OpEnum_s101_server_shutdown;
	public static String OpEnum_s2_loading_client;
	public static String OpEnum_s2_loading_server;
	public static String OpEnum_s3_update_ui_after_startup;
	public static String OpEnum_s3_update_ui_savepoint_after_startup;
	public static String OpEnum_s4_done_startup_client;
	public static String OpEnum_s4_done_startup_server;
	public static String OpEnum_s5_client_connected;
	public static String OpEnum_s6_client_disconnected;
	public static String OpEnum_s7_config_updated;
	public static String OpEnum_s8_server_disconnected;
	public static String OpEnum_s99_client_shutdown;
	public static String OpEnum_s99_server_shutdown;
	public static String OpEnum_scaning_file_list;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
