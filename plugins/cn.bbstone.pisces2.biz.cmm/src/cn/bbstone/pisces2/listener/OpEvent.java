package cn.bbstone.pisces2.listener;

public class OpEvent {
    private OpEnum op;
    /** different op type with different jsonMsg, parse jsonMsg by customer code on need **/
    private String jsonMsg;

    public OpEvent(OpEnum op, String jsonMsg) {
        this.op = op;
        this.jsonMsg = jsonMsg;
    }

    public OpEnum getOp() {
        return op;
    }

    public void setOp(OpEnum op) {
        this.op = op;
    }

    public String getJsonMsg() {
        return jsonMsg;
    }

    public void setJsonMsg(String jsonMsg) {
        this.jsonMsg = jsonMsg;
    }
}
