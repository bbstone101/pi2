package cn.bbstone.pisces2.listener;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OpUtil {
    private static  final Logger log = LoggerFactory.getLogger(OpUtil.class);

    private static Map<OpEnum, OpListener> opListenerMap = new HashMap<>();

    public static void addListener(OpEnum opEnum, OpListener opListener) {
        opListenerMap.put(opEnum, opListener);
    }

    public static OpListener getListener(OpEnum opEnum) {
        return opListenerMap.get(opEnum);
    }

    public static void execListener(OpEnum opEnum) {
    	execListener(opEnum, null);
    }
    public static void execListener(OpEnum opEnum, String jsonMsg) {
        OpListener opListener = OpUtil.getListener(opEnum);
        if (opListener == null) {
            log.error(String.format("not found listener for op: %s ", opEnum.name()));
            return;
        }
        opListener.operate(new OpEvent(opEnum, jsonMsg));
    }

    public static int size() {
        return opListenerMap.size();
    }

}
