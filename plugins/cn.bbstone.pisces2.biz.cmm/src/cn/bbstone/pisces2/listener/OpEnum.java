package cn.bbstone.pisces2.listener;

public enum OpEnum {
	
	loading_file_list(Messages.OpEnum_loading_file_list),
	
	
	s101_server_shutdown(Messages.OpEnum_s101_server_shutdown),
	s101_client_shutdown(Messages.OpEnum_s101_client_shutdown),
	
	
    s1_initial(Messages.OpEnum_s1_initial),

    s2_loading_server(Messages.OpEnum_s2_loading_server),
    s2_loading_client(Messages.OpEnum_s2_loading_client),

    s3_update_ui_savepoint_after_startup(Messages.OpEnum_s3_update_ui_savepoint_after_startup),
    s3_update_ui_after_startup(Messages.OpEnum_s3_update_ui_after_startup),

    s4_done_startup_server(Messages.OpEnum_s4_done_startup_server),
    s4_done_startup_client(Messages.OpEnum_s4_done_startup_client),

    s5_client_connected(Messages.OpEnum_s5_client_connected),
    s6_client_disconnected(Messages.OpEnum_s6_client_disconnected),
    s7_config_updated(Messages.OpEnum_s7_config_updated),
    
    s8_server_disconnected("server disconnected."),
    
    s99_server_shutdown(Messages.OpEnum_s99_server_shutdown),
    s99_client_shutdown(Messages.OpEnum_s99_client_shutdown),

    client_biz_meta_created(Messages.OpEnum_client_biz_meta_created),
    client_biz_meta_loaded(Messages.OpEnum_client_biz_meta_loaded),
    
    scaning_file_list(Messages.OpEnum_scaning_file_list),
    done_scan_file_list(Messages.OpEnum_done_scan_file_list),
    done_loading_file_list(Messages.OpEnum_done_loading_file_list),
    
    
    
    
    
    Other(Messages.OpEnum_Other);


    private String displayText;

    OpEnum(String displayText) {
        this.displayText = displayText;
    }

    public String getDisplayText() {
        return this.displayText;
    }

}
