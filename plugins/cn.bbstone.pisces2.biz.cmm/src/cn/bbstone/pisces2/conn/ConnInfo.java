package cn.bbstone.pisces2.conn;

/**
 * Connection Information
 * 
 * @author bbstone
 *
 */
public class ConnInfo {

	private int serverPort;
	private String serverHost;
//	private List<String> clientAddrList = new ArrayList<>();
//	private ConcurrentHashMap<String, String> concurrentHashMap = new ConcurrentHashMap<>();

	public int getServerPort() {
		return serverPort;
	}

	public void setServerPort(int serverPort) {
		this.serverPort = serverPort;
	}

	public String getServerHost() {
		return serverHost;
	}

	public void setServerHost(String serverHost) {
		this.serverHost = serverHost;
	}

//	public List<String> getClientAddrList() {
//		return clientAddrList;
//	}
//
//	public void setClientAddrList(List<String> clientAddrList) {
//		this.clientAddrList = clientAddrList;
//	}
	
//	public void addClientAddr(String clientAddr) {
//		concurrentHashMap.put(clientAddr, clientAddr);
//	}
//	
//	public String removeClientAddr(String clientAddr) {
//		return concurrentHashMap.remove(clientAddr);
//	}
//	
//	public Iterator<String> getClientAddrIterator() {
//		return concurrentHashMap.keySet().iterator();
//	}

}
