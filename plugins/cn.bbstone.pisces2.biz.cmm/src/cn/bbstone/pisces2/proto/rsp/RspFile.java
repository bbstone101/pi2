package cn.bbstone.pisces2.proto.rsp;

public class RspFile {
	protected Byte msgType;
	protected String msgId;
	protected long reqTs;
	protected long rspTs;
	protected long fileNo;// item line num in fli.idx
	protected long fileSize; // fileNo file's fileSize(unit: Byte)
	protected int chunkNo; //
	protected int chunks;
	protected String checksum; // body checksum
	protected int bodyLen;
	protected byte[] bodyData; // file checksum

	public Byte getMsgType() {
		return msgType;
	}

	public void setMsgType(Byte msgType) {
		this.msgType = msgType;
	}

	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	public long getReqTs() {
		return reqTs;
	}

	public void setReqTs(long reqTs) {
		this.reqTs = reqTs;
	}

	public long getRspTs() {
		return rspTs;
	}

	public void setRspTs(long rspTs) {
		this.rspTs = rspTs;
	}

	public long getFileNo() {
		return fileNo;
	}

	public void setFileNo(long fileNo) {
		this.fileNo = fileNo;
	}

	public long getFileSize() {
		return fileSize;
	}

	public void setFileSize(long fileSize) {
		this.fileSize = fileSize;
	}

	public int getChunkNo() {
		return chunkNo;
	}

	public void setChunkNo(int chunkNo) {
		this.chunkNo = chunkNo;
	}

	public int getChunks() {
		return chunks;
	}

	public void setChunks(int chunks) {
		this.chunks = chunks;
	}

	public String getChecksum() {
		return checksum;
	}

	public void setChecksum(String checksum) {
		this.checksum = checksum;
	}

	public int getBodyLen() {
		return bodyLen;
	}

	public void setBodyLen(int bodyLen) {
		this.bodyLen = bodyLen;
	}

	public byte[] getBodyData() {
		return bodyData;
	}

	public void setBodyData(byte[] bodyData) {
		this.bodyData = bodyData;
	}

}
