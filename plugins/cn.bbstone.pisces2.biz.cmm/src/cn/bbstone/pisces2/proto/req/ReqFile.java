package cn.bbstone.pisces2.proto.req;

public class ReqFile {
	protected Byte msgType;
	protected String msgId; // byte[32]-uuid
	protected long reqTs;
	protected long fileNo; // line number in fli.idx
	protected short bodyLen;
	protected byte[] bodyData; // byte[bodyLen]

	public Byte getMsgType() {
		return msgType;
	}

	public void setMsgType(Byte msgType) {
		this.msgType = msgType;
	}

	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	public long getReqTs() {
		return reqTs;
	}

	public void setReqTs(long reqTs) {
		this.reqTs = reqTs;
	}

	public long getFileNo() {
		return fileNo;
	}

	public void setFileNo(long fileNo) {
		this.fileNo = fileNo;
	}

	public short getBodyLen() {
		return bodyLen;
	}

	public void setBodyLen(short bodyLen) {
		this.bodyLen = bodyLen;
	}

	public byte[] getBodyData() {
		return bodyData;
	}

	public void setBodyData(byte[] bodyData) {
		this.bodyData = bodyData;
	}

}
