package cn.bbstone.pisces2.util;

import cn.bbstone.pisces2.config.ConfigModel;
import cn.bbstone.pisces2.conn.ConnInfo;

public class CtxUtil {
	
    private static ConfigModel configModel = null;
    private static ConnInfo connInfo = new ConnInfo();
    
//    private static AtomicInteger clientCounter = new AtomicInteger();
    
    public static ConfigModel getConfigModel() {
        return configModel;
    }

    public static void setConfigModel(ConfigModel configModel) {
        CtxUtil.configModel = configModel;
    }

//    public static BFileInfo reqNextFile(ChannelHandlerContext ctx) {
//        BFileInfo nextFile = ClientCache.nextFile();
//        log.info("nextFile: {}", nextFile);
//        if (nextFile != null) {
//            log.debug("after list files, req to download nextFile: {}", JSON.toJSONString(nextFile));
//            // FileReq
//            BFileMsg.BFileReq req = BFileUtil.buildReq(BFileCmd.REQ_FILE, nextFile.getFilepath());
//            ctx.write(req);
//            ctx.writeAndFlush(Unpooled.wrappedBuffer(ConstUtil.delimiter.getBytes(CharsetUtil.UTF_8)));
//            return nextFile;
//        }
//        return null;
//    }

//    public static void addConnInfo(String clientAddress) {
//    	connInfo.setServerPort(configModel.getServerPort());
//    	connInfo.setServerHost(configModel.getServerHost());
//    	
//    	connInfo.addClientAddr(clientAddress);
////    	connInfo.getClientAddrList().add(clientAddress);
////    	clientCounter.incrementAndGet();
//    }
//    
//    public static void removeConnInfo(String clientAddress) {
//    	connInfo.removeClientAddr(clientAddress);
////    	connInfo.getClientAddrList().remove(clientAddress);
////    	clientCounter.decrementAndGet();
//    }
    
    public static ConnInfo getConnInfo() {
    	 return connInfo;
    }


}
