package cn.bbstone.pisces2.util;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestUtil {
	private static Logger log = LoggerFactory.getLogger(TestUtil.class);

    public static void main(String[] args) throws IOException {
//        FileReader fileReader = new FileReader(new File("/Users/bbstone/test/file2temp.txt"));
//        char[] data = new char[252428];
//        int len = fileReader.read(data);
//
//        FileReader fileReader2 = new FileReader(new File("/Users/bbstone/test/file2server.txt"));
//        char[] data2 = new char[252428];
//        int len2 = fileReader.read(data);
//
//        for (int j = 0; j < data.length; j++) {
//            log.info("data[i]: {}, data2[i]: {}", data[j], data2[j]);
//            if (data[j] != data2[j]) {
//                log.info("char in tempFile not same as serverFile in same location: {}", (j + 1));
//                break;
//            }
//        }
//        fileReader.close();
//        fileReader2.close();

        String str = "Hello,World!";
        byte[] strBytes = BByteUtil.toBytes(str);
        String strBytesHex = CipherUtil.hex(strBytes);
        byte[] b1 = new byte[6];
        System.arraycopy(strBytes, 0, b1, 0, 6);
        byte[] b2 = new byte[6];
        System.arraycopy(strBytes, 6, b2, 0, 6);

        byte[] b3 = new byte[strBytes.length];
        System.arraycopy(b1, 0, b3, 0, b1.length);
        System.arraycopy(b2, 0, b3, b1.length, b2.length);

        log.info("b3: {}", BByteUtil.toStr(b3));

    }
}
