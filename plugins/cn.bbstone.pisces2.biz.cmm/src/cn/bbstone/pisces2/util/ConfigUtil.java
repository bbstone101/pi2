package cn.bbstone.pisces2.util;

import cn.bbstone.pisces2.comm.Const;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConfigUtil {
    private static Logger log = LoggerFactory.getLogger(ConfigUtil.class);
    public static int recvBufferSizeInt(String bufSize) {
        try {
            String configBufferSize = bufSize;
            String bufferSizeNum = configBufferSize.substring(0, configBufferSize.length() - 1);
            if (configBufferSize.endsWith("M") || configBufferSize.endsWith("m")) {
                return Integer.valueOf(bufferSizeNum) * 1024 * 1024;
            } else if (configBufferSize.endsWith("K") || configBufferSize.endsWith("k")) {
                return Integer.valueOf(bufferSizeNum) * 1024;
            }
        } catch (Exception e) {
            log.warn(String.format("get client receive buffer size error, will used default value(%d MB).", Const.DEFAULT_CLIENT_RECV_BUFFER_SIZE / (1024*1024)), e);
        }
        return Const.DEFAULT_CLIENT_RECV_BUFFER_SIZE;
    }

}
