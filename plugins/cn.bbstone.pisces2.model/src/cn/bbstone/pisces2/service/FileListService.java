package cn.bbstone.pisces2.service;

import java.util.List;

import cn.bbstone.pisces2.model.FileListItem;

public interface FileListService {
	
	public void loadAll(List<FileListItem> targetList);
	
	public List<FileListItem> loadPage(long start, int pageSize);
	
	public List<FileListItem> loadClientPage(long start, int pageSize);
	
}
