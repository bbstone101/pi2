package cn.bbstone.pisces2.service;

import cn.bbstone.pisces2.model.Config;

public interface ConfigService {

	
	public Config load();
	
	public void store(Config config);
	
}
