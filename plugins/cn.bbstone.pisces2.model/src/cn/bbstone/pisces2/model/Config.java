package cn.bbstone.pisces2.model;

public class Config {

	// --- common/server fields
	private String host;
	private int port;
	private String filter;
	private String rootPath;

	// client fields
//	private String clientDir;
	private String postfix;
	private boolean overwrite;
	private int connNum;
	private String recvBuffer;

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getFilter() {
		return filter;
	}

	public void setFilter(String filter) {
		this.filter = filter;
	}


	public String getRootPath() {
		return rootPath;
	}

	public void setRootPath(String rootPath) {
		this.rootPath = rootPath;
	}

	public String getPostfix() {
		return postfix;
	}

	public void setPostfix(String postfix) {
		this.postfix = postfix;
	}

	public boolean isOverwrite() {
		return overwrite;
	}

	public void setOverwrite(boolean overwrite) {
		this.overwrite = overwrite;
	}

	public int getConnNum() {
		return connNum;
	}

	public void setConnNum(int connNum) {
		this.connNum = connNum;
	}

	public String getRecvBuffer() {
		return recvBuffer;
	}

	public void setRecvBuffer(String recvBuffer) {
		this.recvBuffer = recvBuffer;
	}

}
