package cn.bbstone.pisces2.model;

/**
 * List Item(line) in: ~/.fli_server/fli.idx or ~/.fli_client/fli.idx
 * 
 * @author bbstone
 *
 */
public class FileListItem {

	private long fileNo;
	private String fileCat;
	private String rpath;

	private boolean delivered;

	public long getFileNo() {
		return fileNo;
	}

	public void setFileNo(long fileNo) {
		this.fileNo = fileNo;
	}

	public String getFileCat() {
		return fileCat;
	}

	public void setFileCat(String fileCat) {
		this.fileCat = fileCat;
	}

	public String getRpath() {
		return rpath;
	}

	public void setRpath(String rpath) {
		this.rpath = rpath;
	}

	public boolean isDelivered() {
		return delivered;
	}

	public void setDelivered(boolean delivered) {
		this.delivered = delivered;
	}

}
