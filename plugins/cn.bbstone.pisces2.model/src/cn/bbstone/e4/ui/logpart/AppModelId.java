package cn.bbstone.e4.ui.logpart;

public class AppModelId {
	public static final String COMMAND_CN_BBSTONE_E4_UI_COMMAND_LOG_CLEAR_CO = "cn.bbstone.e4.ui.command.log.clear_co";
	public static final String COMMAND_CN_BBSTONE_E4_UI_COMMAND_LOG_LOCK_CO = "cn.bbstone.e4.ui.command.log.lock_co";
	public static final String COMMAND_CN_BBSTONE_E4_UI_COMMAND_LOG_WORDWRAP = "cn.bbstone.e4.ui.command.log.wordwrap";
	public static final String COMMAND_CN_BBSTONE_E4_UI_COMMAND_TESTLOG = "cn.bbstone.e4.ui.command.testlog";
	public static final String HANDLEDTOOLITEM_CN_BBSTONE_E4_UI_LOGPART_HANDLEDTOOLITEM_LOGSCLEAN = "cn.bbstone.e4.ui.logpart.handledtoolitem.logsclean";
	public static final String HANDLEDTOOLITEM_CN_BBSTONE_E4_UI_LOGPART_HANDLEDTOOLITEM_LOGSLOCK = "cn.bbstone.e4.ui.logpart.handledtoolitem.logslock";
	public static final String HANDLEDTOOLITEM_CN_BBSTONE_E4_UI_LOGPART_HANDLEDTOOLITEM_LOGSWRAP = "cn.bbstone.e4.ui.logpart.handledtoolitem.logswrap";
	public static final String PART_CN_BBSTONE_E4_UI_PART_LOGS = "cn.bbstone.e4.ui.part.logs";
	public static final String TOOLBAR_CN_BBSTONE_E4_UI_TOOLBAR_LOGS = "cn.bbstone.e4.ui.toolbar.logs";
}