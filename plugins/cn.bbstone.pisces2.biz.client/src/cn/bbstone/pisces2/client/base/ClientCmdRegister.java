package cn.bbstone.pisces2.client.base;


import cn.bbstone.pisces2.client.handler.ClientHandler;
import cn.bbstone.pisces2.client.handler.RspFileDataHandler;
import cn.bbstone.pisces2.client.handler.RspFileInfoHandler;
import cn.bbstone.pisces2.client.handler.RspListDataHandler;
import cn.bbstone.pisces2.client.handler.RspListInfoHandler;
import cn.bbstone.pisces2.comm.BFileCmd;

import java.util.HashMap;
import java.util.Map;

/**
 * NOTICE: multi-client will execute this
 */
public class ClientCmdRegister {
    private static boolean initialed = false;
    private static final Map<Byte, ClientHandler> cmdHandlerMap = new HashMap<>();

    public synchronized static void init() {
        if (!initialed) {
            register(BFileCmd.RSP_LIST_INFO, new RspListInfoHandler());
            register(BFileCmd.RSP_LIST_DATA, new RspListDataHandler());
            register(BFileCmd.RSP_FILE_INFO, new RspFileInfoHandler());
            register(BFileCmd.RSP_FILE_DATA, new RspFileDataHandler());
            initialed = true;
        }

    }

    private static void register(Byte cmd, ClientHandler clientHandler) {
        cmdHandlerMap.put(cmd, clientHandler);
    }

    public static ClientHandler getHandler(Byte cmd) {
        return cmdHandlerMap.get(cmd);
    }
}
