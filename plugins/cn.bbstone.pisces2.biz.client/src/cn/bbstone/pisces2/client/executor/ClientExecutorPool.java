package cn.bbstone.pisces2.client.executor;

import cn.bbstone.pisces2.config.Config;
import cn.bbstone.pisces2.util.CtxUtil;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ClientExecutorPool {
    private static ExecutorService executorService = Executors.newFixedThreadPool(CtxUtil.getConfigModel().getConnNum());

    public static void submit(ClientExecutor clientExecutor) {
        executorService.submit(clientExecutor);
    }
}
