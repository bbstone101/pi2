package cn.bbstone.pisces2.client.executor;

import cn.bbstone.pisces2.client.ClientStarter;
import cn.bbstone.pisces2.client.FileClient;
import cn.hutool.core.util.IdUtil;

public class ClientExecutor implements Runnable{

	@Override
    public void run() {
        FileClient fileClient = new FileClient();
        ClientStarter.addClient(IdUtil.fastSimpleUUID(), fileClient);
        fileClient.startup();
    }
}
