package cn.bbstone.pisces2.client.executor;

import cn.bbstone.pisces2.client.util.MsgUtil;
import io.netty.channel.ChannelHandlerContext;

public class TaskExecutor implements Runnable{

    private ChannelHandlerContext ctx;

    public TaskExecutor(ChannelHandlerContext ctx) {
        this.ctx = ctx;
    }

    @Override
    public void run() {
        MsgUtil.reqNextFileInfo(ctx);
    }
}
