package cn.bbstone.pisces2.client.task.event;

public class FailEventData {
    private long fileNo;
    private String reason;

    public FailEventData() {
    }
    public FailEventData(long fileNo, String reason) {
        this.fileNo = fileNo;
        this.reason = reason;
    }

    public long getFileNo() {
        return fileNo;
    }

    public void setFileNo(long fileNo) {
        this.fileNo = fileNo;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
