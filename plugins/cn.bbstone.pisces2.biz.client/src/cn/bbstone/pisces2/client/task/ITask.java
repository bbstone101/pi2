package cn.bbstone.pisces2.client.task;


import cn.bbstone.pisces2.comm.StatusEnum;
import cn.bbstone.pisces2.proto.rsp.RspFile;

public interface ITask {
    StatusEnum appendFileData(RspFile rspFile);
}
