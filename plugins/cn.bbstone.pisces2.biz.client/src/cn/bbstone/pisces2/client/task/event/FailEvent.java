package cn.bbstone.pisces2.client.task.event;

public class FailEvent {
    private String code;
    private FailEventData failEventData;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public FailEventData getFailEventData() {
        return failEventData;
    }

    public void setFailEventData(FailEventData failEventData) {
        this.failEventData = failEventData;
    }
}
