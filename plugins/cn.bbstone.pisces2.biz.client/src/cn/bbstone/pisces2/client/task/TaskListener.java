package cn.bbstone.pisces2.client.task;


import cn.bbstone.pisces2.client.task.event.FailEvent;
import cn.bbstone.pisces2.comm.fli.FliSavePoint;

public interface TaskListener {
    /**
     * TODO optimize save piont file update logic onCompleted method invoked
     *
     * @param fliSavePoint
     */
    void onCompleted(FliSavePoint fliSavePoint);
    
    void onSkip(FliSavePoint fliSavePoint);

    void onFail(FailEvent failEvent);
}
