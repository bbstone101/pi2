package cn.bbstone.pisces2.client.handler;

import cn.bbstone.pisces2.client.task.impl.FileTask;
import cn.bbstone.pisces2.proto.rsp.RspFile;
import io.netty.channel.ChannelHandlerContext;

public interface ClientHandler {

    void handle(ChannelHandlerContext ctx, RspFile rspFile);

    void handleNext(ChannelHandlerContext ctx, RspFile rspFile, FileTask fileTask);


}
