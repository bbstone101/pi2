package cn.bbstone.pisces2.server.handler;

import cn.bbstone.pisces2.proto.req.ReqFile;
import cn.bbstone.pisces2.server.cache.FileDataCacheItem;
import cn.bbstone.pisces2.server.cache.ServerCache;
import io.netty.channel.ChannelHandlerContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReqFileInfoAckHandler implements ServerHandler {
    private static Logger log = LoggerFactory.getLogger(ReqFileInfoAckHandler.class);

    @Override
    public void handle(ChannelHandlerContext ctx, ReqFile reqFile) {
//        long fileNo = reqFile.getFileNo();
//        log.info("file(with fielNo: {}) information success recv by client.", fileNo);
        // TODO setup reqListInfo recv timeout, if timeout not recv this msg, try to resend reqListInfo


        long fileNo = reqFile.getFileNo();
        FileDataCacheItem cacheItem = ServerCache.get(fileNo);
        log.debug("file info ack, fileNo: {}, chunks: {}, chunkNo: {}", fileNo, cacheItem.getChunks(), cacheItem.getChunkNo());


    }
}