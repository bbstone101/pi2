package cn.bbstone.pisces2.server.handler;

import cn.bbstone.pisces2.comm.BFileCmd;
import cn.bbstone.pisces2.proto.req.ReqFile;
import cn.bbstone.pisces2.server.util.MsgUtil;
import io.netty.channel.ChannelHandlerContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReqFileDataHandler implements ServerHandler {
    private static Logger log = LoggerFactory.getLogger(ReqFileDataHandler.class);

    public ReqFileDataHandler() {
    }

    @Override
    public void handle(ChannelHandlerContext ctx, ReqFile reqFile) {
        long fileNo = reqFile.getFileNo();
        Byte reqMsgType = reqFile.getMsgType();
        long reqTs = reqFile.getReqTs();
        log.debug("reqMsgType: {}, fileNo: {}", reqMsgType, fileNo);
        MsgUtil.sendFileData(ctx, BFileCmd.RSP_FILE_DATA, fileNo, reqTs);
    }


}
