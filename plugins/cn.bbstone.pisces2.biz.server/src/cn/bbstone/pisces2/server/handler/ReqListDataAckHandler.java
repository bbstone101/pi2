package cn.bbstone.pisces2.server.handler;

import cn.bbstone.pisces2.comm.BFileCmd;
import cn.bbstone.pisces2.proto.req.ReqFile;
import cn.bbstone.pisces2.server.cache.FileDataCacheItem;
import cn.bbstone.pisces2.server.cache.ServerCache;
import cn.bbstone.pisces2.server.util.MsgUtil;
import io.netty.channel.ChannelHandlerContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReqListDataAckHandler implements ServerHandler {
    private static Logger log = LoggerFactory.getLogger(ReqListDataAckHandler.class);

    public ReqListDataAckHandler() {
    }

    @Override
    public void handle(ChannelHandlerContext ctx, ReqFile reqFile) {
        long fileNo = reqFile.getFileNo();
        FileDataCacheItem cacheItem = ServerCache.get(fileNo);
        log.debug("list data ack, fileNo: {}, chunks: {}, chunkNo: {}", fileNo, cacheItem.getChunks(), cacheItem.getChunkNo());
        // all file data chunks have been sent, remove cacheItem
        if (cacheItem.getChunks() <= cacheItem.getChunkNo()) {
            MsgUtil.closeServerFis(cacheItem);
            ServerCache.remove(cacheItem.getFileNo());
            log.debug("now serverCache.size: {}", ServerCache.size());
        } else { // send next chunk
            MsgUtil.sendFileData(ctx, BFileCmd.RSP_LIST_DATA, fileNo, reqFile.getReqTs());
        }

    }
}
