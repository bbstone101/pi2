package cn.bbstone.pisces2.server.handler;

import cn.bbstone.pisces2.proto.req.ReqFile;
import cn.bbstone.pisces2.server.cache.FileDataCacheItem;
import cn.bbstone.pisces2.server.cache.ServerCache;
import cn.bbstone.pisces2.server.util.MsgUtil;
import io.netty.channel.ChannelHandlerContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReqFileInfoSkipAckHandler implements ServerHandler {
    private static Logger log = LoggerFactory.getLogger(ReqFileInfoSkipAckHandler.class);

    public ReqFileInfoSkipAckHandler() {
    }

    @Override
    public void handle(ChannelHandlerContext ctx, ReqFile reqFile) {

        long fileNo = reqFile.getFileNo();
        FileDataCacheItem cacheItem = ServerCache.get(fileNo);
        log.info("file info skip ack, fileNo: {}, chunks: {}, chunkNo: {}", fileNo, cacheItem.getChunks(), cacheItem.getChunkNo());
//        if (cacheItem == null) {
//            throw new RuntimeException(String.format("not foud cacheItem for fileNo: %d", fileNo));
//        }
        MsgUtil.closeServerFis(cacheItem);
        ServerCache.remove(cacheItem.getFileNo());
        log.debug("now (FileDataCacheItem)serverCache.size: {}", ServerCache.size());


    }
}
