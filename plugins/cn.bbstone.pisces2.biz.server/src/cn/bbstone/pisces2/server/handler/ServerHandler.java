package cn.bbstone.pisces2.server.handler;

import cn.bbstone.pisces2.proto.req.ReqFile;
import io.netty.channel.ChannelHandlerContext;

public interface ServerHandler {

    public void handle(ChannelHandlerContext ctx, ReqFile reqFile);

}
