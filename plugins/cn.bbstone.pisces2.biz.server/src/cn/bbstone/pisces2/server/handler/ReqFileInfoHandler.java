package cn.bbstone.pisces2.server.handler;

import cn.bbstone.pisces2.comm.BFileCmd;
import cn.bbstone.pisces2.proto.req.ReqFile;
import cn.bbstone.pisces2.server.util.MsgUtil;
import io.netty.channel.ChannelHandlerContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReqFileInfoHandler implements ServerHandler {
    private static Logger log = LoggerFactory.getLogger(ReqFileInfoHandler.class);

    public ReqFileInfoHandler() {
    }

    @Override
    public void handle(ChannelHandlerContext ctx, ReqFile reqFile) {
        long fileNo = reqFile.getFileNo();
        Byte msgType = reqFile.getMsgType();
        long reqTs = reqFile.getReqTs();
        log.debug("recv req msgId: {}, msgType: {}, fileNo: {}, reqTs: {}", reqFile.getMsgId(), msgType, fileNo, reqTs);
        // ---------------------- read server fli index file info
        try {
            MsgUtil.sendFileInfo(ctx, BFileCmd.RSP_FILE_INFO, fileNo, reqTs);
        } catch (Exception e) {
            log.error("handler FILE INFO REQ error.", e);
        }
    }
}
