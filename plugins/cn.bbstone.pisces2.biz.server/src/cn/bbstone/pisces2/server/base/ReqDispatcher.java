package cn.bbstone.pisces2.server.base;

import cn.bbstone.pisces2.proto.req.ReqFile;
import cn.bbstone.pisces2.server.handler.ServerHandler;
import io.netty.channel.ChannelHandlerContext;

/**
 * client request will dispatch to different server cmdHandler according cmd
 *
 */
public class ReqDispatcher {

    public static void dispatch(ChannelHandlerContext ctx, ReqFile reqFile) {
        ServerHandler serverHandler = ServerCmdRegister.getHandler(reqFile.getMsgType());
        serverHandler.handle(ctx, reqFile);
    }

}
