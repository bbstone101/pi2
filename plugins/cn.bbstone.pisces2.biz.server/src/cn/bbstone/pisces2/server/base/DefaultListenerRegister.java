package cn.bbstone.pisces2.server.base;

import cn.bbstone.pisces2.listener.IListenerRegister;
import cn.bbstone.pisces2.listener.OpEnum;
import cn.bbstone.pisces2.listener.OpUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DefaultListenerRegister implements IListenerRegister {
    private static final Logger log = LoggerFactory.getLogger(DefaultListenerRegister.class);

    @Override
    public void register() {
        for (OpEnum opEnum : OpEnum.values()) {
            OpUtil.addListener(opEnum,
                    opEvent -> {
                        log.info("opMsg: {}", opEvent.getOp().getDisplayText());
                    });
        }
        log.info("total register opListeners: {}", OpUtil.size());
    }
}
