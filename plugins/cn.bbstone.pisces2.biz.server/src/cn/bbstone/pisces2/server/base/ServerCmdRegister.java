package cn.bbstone.pisces2.server.base;


import cn.bbstone.pisces2.comm.BFileCmd;
import cn.bbstone.pisces2.server.handler.ServerHandler;
import cn.bbstone.pisces2.server.handler.*;

import java.util.HashMap;
import java.util.Map;

public class ServerCmdRegister {

    private static final Map<Byte, ServerHandler> cmdHandlerMap = new HashMap<>();

    public static void init() {
        register(BFileCmd.REQ_LIST_INFO, new ReqListInfoHandler());
        register(BFileCmd.REQ_LIST_INFO_ACK, new ReqListInfoAckHandler());

        register(BFileCmd.REQ_FILE_INFO, new ReqFileInfoHandler());
        register(BFileCmd.REQ_FILE_INFO_ACK, new ReqFileInfoAckHandler());
        register(BFileCmd.REQ_FILE_INFO_SKIP_ACK, new ReqFileInfoSkipAckHandler());

        register(BFileCmd.REQ_LIST_DATA, new ReqListDataHandler());
        register(BFileCmd.REQ_LIST_DATA_ACK, new ReqListDataAckHandler());

        register(BFileCmd.REQ_FILE_DATA, new ReqFileDataHandler());
        register(BFileCmd.REQ_FILE_DATA_ACK, new ReqFileDataAckHandler());


    }

    public static void register(Byte cmd, ServerHandler serverHandler) {
        cmdHandlerMap.put(cmd, serverHandler);
    }

    public static ServerHandler getHandler(Byte cmd) {
        return cmdHandlerMap.get(cmd);
    }

}
