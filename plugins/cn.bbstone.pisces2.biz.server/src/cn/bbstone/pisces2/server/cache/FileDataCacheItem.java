package cn.bbstone.pisces2.server.cache;

import java.io.FileInputStream;

/**
 * The file data cache item info
 */
public class FileDataCacheItem {
    private FileInputStream fileInputStream;
    private String fileId; // file checksum
    private String fileAbsPath; // fileLen
    private long nextReadPos;
    private long fileNo;
    private long fileSize; // same as fileLen
    private int chunks; // total chunks of the file data
    private int chunkNo;
    private int chunkSize; // latest chunk bytes size

    public FileInputStream getFileInputStream() {
        return fileInputStream;
    }

    public void setFileInputStream(FileInputStream fileInputStream) {
        this.fileInputStream = fileInputStream;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getFileAbsPath() {
        return fileAbsPath;
    }

    public void setFileAbsPath(String fileAbsPath) {
        this.fileAbsPath = fileAbsPath;
    }

    public long getNextReadPos() {
        return nextReadPos;
    }

    public void setNextReadPos(long nextReadPos) {
        this.nextReadPos = nextReadPos;
    }

    public long getFileNo() {
        return fileNo;
    }

    public void setFileNo(long fileNo) {
        this.fileNo = fileNo;
    }

    public long getFileSize() {
        return fileSize;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    public int getChunks() {
        return chunks;
    }

    public void setChunks(int chunks) {
        this.chunks = chunks;
    }

    public int getChunkNo() {
        return chunkNo;
    }

    public void setChunkNo(int chunkNo) {
        this.chunkNo = chunkNo;
    }

    public int getChunkSize() {
        return chunkSize;
    }

    public void setChunkSize(int chunkSize) {
        this.chunkSize = chunkSize;
    }
}
