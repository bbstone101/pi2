package cn.bbstone.pisces2.server.cache;

import cn.bbstone.pisces2.comm.fli.FliSavePoint;

import java.util.HashMap;
import java.util.Map;

public class ServerCache {
    // K: fileId(md5Hex(File),
    private static Map<Long, FileDataCacheItem> fileDataCache = new HashMap<>();

    public static FliSavePoint fliSavePoint = null;

    public static void setFliSavePoint(FliSavePoint savePoint) {
        fliSavePoint = savePoint;
    }

    public static long getSpCount() {
        return fliSavePoint == null ? 0L : fliSavePoint.getCount();
    }

    public static void put(Long fileNo, FileDataCacheItem item) {
        fileDataCache.put(fileNo, item);
    }

    public static FileDataCacheItem get(Long fileNo) {
        return fileDataCache.get(fileNo);
    }

    public static void remove(Long fileNo) {
        fileDataCache.remove(fileNo);
    }


    /**
     * fileDataCache size
     */
    public static int size() {
        return fileDataCache.size();
    }
}
