package cn.bbstone.pisces2.server;

import cn.bbstone.pisces2.comm.cache.ServerFliIndexCache;
import cn.bbstone.pisces2.config.ConfigModel;
import cn.bbstone.pisces2.listener.IListenerRegister;
import cn.bbstone.pisces2.server.base.DefaultListenerRegister;

public class ServerStarter {
	static FileServer server;

	public static void main(String[] args) {
		startup(null, new DefaultListenerRegister());
	}

	public static void startup(ConfigModel configModel, IListenerRegister listenerRegister) {
		long maxMem = Runtime.getRuntime().maxMemory() / (1024 * 1024);
		if (maxMem < 32) {// 32MB
			throw new RuntimeException("memory minimal 32 MB.");
		}
		// reset key status
		resetStatus();
		server = new FileServer();
		server.startup(configModel, listenerRegister);
	}

	public static void shutdown() {
		if (server != null)
			server.shutdown();
	}
	
	private static boolean resetStatus() {
		boolean bool = false;
		// reset cache current pos
		ServerFliIndexCache.resetCurrentPos();
		
		return bool;
	}
	
}
