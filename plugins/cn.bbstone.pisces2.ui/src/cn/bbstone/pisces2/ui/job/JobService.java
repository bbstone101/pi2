package cn.bbstone.pisces2.ui.job;

import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.e4.ui.di.UISynchronize;

public interface JobService {
	
	public Job newJob(UISynchronize sync, String jobName, IJobTask task, Runnable callback);
	
	public Job newJobWithProgress(UISynchronize sync, String jobName, String taskName, IJobTask task, Runnable callback);

	public void cancelJobs();
}
