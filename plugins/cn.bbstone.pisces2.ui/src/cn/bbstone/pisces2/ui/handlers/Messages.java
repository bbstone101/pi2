package cn.bbstone.pisces2.ui.handlers;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = Messages.class.getPackageName() + ".messages"; //$NON-NLS-1$
	public static String dialog_content_about;
	public static String dialog_content_client_running_exit;
	public static String dialog_content_exit;
	public static String dialog_content_return_with_notempty_client_root;
	public static String dialog_content_server_running_exit;
	public static String dialog_content_shutdown_client;
	public static String dialog_content_shutdown_client_notstart;
	public static String dialog_content_shutdown_server;
	public static String dialog_content_shutdown_server_notstart;
	public static String dialog_content_shutdown_with_client_conn;
	public static String dialog_content_startup_client_stated_conflict;
	public static String dialog_content_startup_server_stated_conflict;
	public static String dialog_title_about;
	public static String dialog_title_confirm;
	
	public static String dialog_title_server_started;
	public static String dialog_content_server_started;
	public static String dialog_title_client_started;
	public static String dialog_content_client_started;
	
	
	public static String help_dev_link;
	public static String help_guide_link;
	
	
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
