package cn.bbstone.pisces2.ui.handlers;

import javax.inject.Inject;
import javax.inject.Named;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.e4.ui.workbench.IWorkbench;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.bbstone.e4.ui.log.service.LogsService;
import cn.bbstone.pisces2.client.ClientStarter;
import cn.bbstone.pisces2.cmm.Pi2ContextUtil;
import cn.bbstone.pisces2.cmm.UIConst;
import cn.bbstone.pisces2.listener.OpEnum;
import cn.bbstone.pisces2.server.ServerStarter;
import cn.bbstone.pisces2.ui.base.enums.CmdEnum;
import cn.bbstone.pisces2.ui.base.notify.Pi2Events;
import cn.bbstone.pisces2.ui.job.JobService;

public class ExitHandler {
	private static Logger log = LoggerFactory.getLogger(ExitHandler.class);

	@Inject
	IEventBroker eventBroker;

	@Inject
	private JobService jobService;

	@Inject
	private LogsService logsService;

	private boolean canExecuted = true;


	private void disabledButton() {
		this.canExecuted = false;
	}
	
	private void enabledButton() {
		this.canExecuted = true;
	}
	
	
	@CanExecute
	public boolean canExecute() {
//		log.info("exit-canExecuted: {}", this.canExecuted);
		return this.canExecuted;
	}

	@Execute
	public void execute(IWorkbench workbench, @Named(IServiceConstants.ACTIVE_SHELL) Shell shell) {
		log.info((this.getClass().getSimpleName() + " called")); //$NON-NLS-1$

		if (UIConst.RUN_AS_SERVER.equals(Pi2ContextUtil.getRunAs())) {
			if (Pi2ContextUtil.isRunning()) {
				// runAs client
				if (MessageDialog.openConfirm(shell, Messages.dialog_title_confirm,
						Messages.dialog_content_server_running_exit)) {
					doCloseServer(workbench, shell);
				}
			} else {
				if (MessageDialog.openConfirm(shell, Messages.dialog_title_confirm, Messages.dialog_content_exit)) {
					doCloseServer(workbench, shell);
				}
			}

		}
		// runAs client
		if (UIConst.RUN_AS_CLIENT.equals(Pi2ContextUtil.getRunAs())) {
			if (Pi2ContextUtil.isRunning()) {
				if (MessageDialog.openConfirm(shell, Messages.dialog_title_confirm,
						Messages.dialog_content_client_running_exit)) {
					doCloseClient(workbench, shell);
				}

			} else {
				if (MessageDialog.openConfirm(shell, Messages.dialog_title_confirm, Messages.dialog_content_exit)) {
					doCloseClient(workbench, shell);
				}
			}
		}
	}

//	private void doClose(IWorkbench workbench, Shell shell) {
//		eventBroker.post(Pi2Events.OPERATION, OpEnum.s101_server_shutdown.getDisplayText());
//		Pi2ContextUtil.reset();
//		jobService.cancelJobs();
//
//		logsService.dispose();
//
//		shell.close();
//		workbench.close();
//	}

	private void doCloseServer(IWorkbench workbench, Shell shell) {
		ServerStarter.shutdown();
		Pi2ContextUtil.setRunning(false);
		eventBroker.post(Pi2Events.OPERATION, OpEnum.s101_server_shutdown.getDisplayText());
		Pi2ContextUtil.reset();
		
		jobService.cancelJobs();
		logsService.dispose();

		shell.close();
		workbench.close();
	}

	private void doCloseClient(IWorkbench workbench, Shell shell) {
		ClientStarter.shutdown();
		Pi2ContextUtil.setRunning(false);
		eventBroker.post(Pi2Events.OPERATION, OpEnum.s101_client_shutdown.getDisplayText());
		Pi2ContextUtil.reset();
		
		jobService.cancelJobs();
		logsService.dispose();
		
//		EventManager.clos
		shell.close();
		workbench.close();
	}
	
	@Inject
	@Optional
	void onStart(final @UIEventTopic(Pi2Events.STARTUP) CmdEnum cmdEnum) {
//		log.info("@ExitHandler handling onStart");
		if (CmdEnum.startup.equals(cmdEnum)) {
			this.disabledButton();
		}
	}
	
	@Inject
	@Optional
	void onStop(final @UIEventTopic(Pi2Events.SHUTDOWN) CmdEnum cmdEnum) {
		if (CmdEnum.stop.equals(cmdEnum)) {
			this.enabledButton();
		}
	}

}
