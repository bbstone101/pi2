 
package cn.bbstone.pisces2.ui.handlers;

import org.eclipse.e4.core.di.annotations.Execute;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PauseHandler {
	private static Logger log = LoggerFactory.getLogger(PauseHandler.class);
	
	@Execute
	public void execute() {
		log.info((this.getClass().getSimpleName() + " called"));
	}
		
}