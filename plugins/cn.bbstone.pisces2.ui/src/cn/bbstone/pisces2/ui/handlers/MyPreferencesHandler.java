//
//package cn.bbstone.pisces2.ui.handlers;
//
//import javax.inject.Inject;
//import javax.inject.Named;
//
//import org.eclipse.e4.core.di.annotations.CanExecute;
//import org.eclipse.e4.core.di.annotations.Execute;
//import org.eclipse.e4.core.di.annotations.Optional;
//import org.eclipse.e4.core.services.events.IEventBroker;
//import org.eclipse.e4.ui.di.UIEventTopic;
//import org.eclipse.e4.ui.model.application.MApplication;
//import org.eclipse.e4.ui.services.IServiceConstants;
//import org.eclipse.jface.preference.PreferenceDialog;
//import org.eclipse.jface.preference.PreferenceManager;
//import org.eclipse.jface.viewers.ViewerComparator;
//import org.eclipse.swt.widgets.Shell;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import cn.bbstone.pisces2.ui.base.enums.CmdEnum;
//import cn.bbstone.pisces2.ui.base.notify.Pi2Events;
//
//public class MyPreferencesHandler {
//	private static Logger log = LoggerFactory.getLogger(MyPreferencesHandler.class);
//	
//	@Inject
//	IEventBroker eventBroker;
//	
//	private boolean canExecuted = true;
//
//
//	private void disabledButton() {
//		this.canExecuted = false;
//	}
//	
//	private void enabledButton() {
//		this.canExecuted = true;
//	}
//	
//	
//	@CanExecute
//	public boolean canExecute() {
//		log.info("pref-canExecuted: {}", this.canExecuted);
//		return this.canExecuted;
//	}
//
//	@Execute
//	public void execute(@Named(IServiceConstants.ACTIVE_SHELL) Shell shell, @Optional PreferenceManager pm,
//			MApplication appli) {
//
//		
//		
////		eventBroker.subscribe(Pi2Events.STARTUP, (event) -> {
////			this.disabledButton();
////		});
//		// Can display the standard dialog.
//		PreferenceDialog dialog = new PreferenceDialog(shell, pm);
//		dialog.create();
//		dialog.getTreeViewer().setComparator(new ViewerComparator());
//		dialog.getTreeViewer().expandAll();
//		dialog.open();
//	}
//	
//	// when start pi2(server/client), preference button disabled
//	@Inject
//	@Optional
//	void onStart(final @UIEventTopic(Pi2Events.STARTUP) CmdEnum cmdEnum) {
//		log.info("@preferenceHandler handle onStart");
//		if (CmdEnum.startup.equals(cmdEnum)) {
//			log.info("cmd enum startup..", cmdEnum.toString());
//			this.disabledButton();
//		}
//	}
//	
//
//}
