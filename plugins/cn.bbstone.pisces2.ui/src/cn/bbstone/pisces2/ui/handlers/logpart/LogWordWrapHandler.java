 
package cn.bbstone.pisces2.ui.handlers.logpart;

import org.eclipse.e4.core.di.annotations.Execute;

import javax.inject.Inject;

import org.eclipse.e4.core.di.annotations.CanExecute;

import cn.bbstone.e4.ui.log.service.LogsService;

public class LogWordWrapHandler {
	
	@Inject
	private LogsService logsService;
	
	@Execute
	public void execute() {
		logsService.handleWordWrap();
	}
	
	
	@CanExecute
	public boolean canExecute() {
		
		return true;
	}
		
}