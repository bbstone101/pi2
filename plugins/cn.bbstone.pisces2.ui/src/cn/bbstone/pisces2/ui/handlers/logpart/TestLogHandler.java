 
package cn.bbstone.pisces2.ui.handlers.logpart;

import javax.inject.Inject;

import org.eclipse.e4.core.di.annotations.Execute;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.bbstone.e4.ui.log.service.LogsService;

public class TestLogHandler {
	private Logger log = LoggerFactory.getLogger(TestLogHandler.class);
	@Inject
	private LogsService logsService;
	
	@Execute
	public void execute() {
		log.info("test log command called.This message only for debug LogPart right upper corner's buttons functions....");
//		log.info((this.getClass().getSimpleName() + " called"));
		logsService.printLog(null);;
	}
		
}