 
package cn.bbstone.pisces2.ui.handlers;

import javax.inject.Named;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.e4.ui.workbench.IWorkbench;
import org.eclipse.swt.widgets.Shell;

import cn.bbstone.pisces2.ui.views.about.AboutBoxDialog;

public class AboutHandler {
//	private static Logger log = LoggerFactory.getLogger(AboutHandler.class);
	
	@Execute
	public void execute(IWorkbench workbench,  @Named (IServiceConstants.ACTIVE_SHELL)Shell shell) {
//		System.out.println((this.getClass().getSimpleName() + " called"));
//		log.info((this.getClass().getSimpleName() + " called")); //$NON-NLS-1$
//		MessageDialog.openInformation(shell, Messages.dialog_title_about, Messages.dialog_content_about);
		
		AboutBoxDialog dialog = new AboutBoxDialog(shell);
        dialog.open();
	}
		
}