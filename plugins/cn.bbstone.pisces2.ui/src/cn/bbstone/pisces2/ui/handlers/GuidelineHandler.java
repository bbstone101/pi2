package cn.bbstone.pisces2.ui.handlers;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.swt.program.Program;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GuidelineHandler {
	private static Logger log = LoggerFactory.getLogger(GuidelineHandler.class);
	
	@CanExecute
	public boolean canExecute() {
		return true;
	}
	
	@Execute
	public void execute() {
//		System.out.println((this.getClass().getSimpleName() + " called"));
		log.info((this.getClass().getSimpleName() + " called"));
		
		Program.launch(Messages.help_guide_link);
	}
	
	
}
