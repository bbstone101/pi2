//package cn.bbstone.pisces2.ui.handlers;
//
//import javax.annotation.PreDestroy;
//import javax.inject.Inject;
//import javax.inject.Named;
//
//import org.eclipse.core.runtime.preferences.IEclipsePreferences;
//import org.eclipse.core.runtime.preferences.IEclipsePreferences.PreferenceChangeEvent;
//import org.eclipse.e4.core.di.annotations.Execute;
//import org.eclipse.e4.core.di.annotations.Optional;
//import org.eclipse.e4.core.di.extensions.Preference;
//import org.eclipse.e4.ui.di.UIEventTopic;
//import org.eclipse.e4.ui.model.application.MApplication;
//import org.eclipse.e4.ui.services.IServiceConstants;
//import org.eclipse.jface.preference.PreferenceManager;
//import org.eclipse.jface.window.Window;
//import org.eclipse.swt.widgets.Shell;
//import org.osgi.service.prefs.BackingStoreException;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import cn.bbstone.pisces2.model.Config;
//import cn.bbstone.pisces2.service.ConfigService;
//import cn.bbstone.pisces2.ui.base.enums.CmdEnum;
//import cn.bbstone.pisces2.ui.base.notify.Pi2Events;
//import cn.bbstone.pisces2.ui.views.ConfigTitleAreaDialog;
//
//public class ConfigHandler {
//	private static Logger log = LoggerFactory.getLogger(ConfigHandler.class);
//
//	@Inject
//	private ConfigService configService;
//
//	private boolean canExecuted = true;
//
//	private void disabledButton() {
//		this.canExecuted = false;
//	}
//
//	private void enabledButton() {
//		this.canExecuted = true;
//	}
//
//	@Execute
//	public void execute(@Preference(nodePath = "cn.bbstone.pisces2.ui") IEclipsePreferences prefs,
//			@Named(IServiceConstants.ACTIVE_SHELL) Shell shell, @Optional PreferenceManager pm, MApplication appli) {
////		System.out.println((this.getClass().getSimpleName() + " called"));
//		log.info((this.getClass().getSimpleName() + " called"));
//
//		prefs.addPreferenceChangeListener(new IEclipsePreferences.IPreferenceChangeListener() {
//			@Override
//			public void preferenceChange(PreferenceChangeEvent event) {
//				if ("port".equals(event.getKey())) {
////					System.out.println("Key: port, Value: old: " + event.getOldValue() + ", new: " + event.getNewValue());
//					log.info("Key: port, Value: old: {}, new: {}", event.getOldValue(), event.getNewValue());
//				}
//			}
//		});
//
//		try {
//			prefs.flush();
//		} catch (BackingStoreException e) {
//		}
//
//		ConfigTitleAreaDialog dialog = new ConfigTitleAreaDialog(shell);
//		dialog.create();
//		Config initConfig = configService.load();
//		dialog.initFormData(initConfig);
//		if (dialog.open() == Window.OK) {
//			Config config = dialog.getConfig();
//			if (config != null) {
//				configService.store(config);
//			}
////			System.out.println(dialog.getFirstName());
////			System.out.println(dialog.getLastName());
//		}
//
//	}
//
//	@PreDestroy
//	public void dispose() {
//
//	}
//	
//	@Inject
//	@Optional
//	void onStart(final @UIEventTopic(Pi2Events.STARTUP) CmdEnum cmdEnum) {
//		log.info("@ConfigHandler handling onStart");
//		if (CmdEnum.startup.equals(cmdEnum)) {
//			this.disabledButton();
//		}
//	}
//	
//	@Inject
//	@Optional
//	void onStop(final @UIEventTopic(Pi2Events.SHUTDOWN) CmdEnum cmdEnum) {
//		if (CmdEnum.stop.equals(cmdEnum)) {
//			this.enabledButton();
//		}
//	}
//	
//	
//
//}
