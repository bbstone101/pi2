package cn.bbstone.pisces2.ui.handlers;
 

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.swt.program.Program;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HelpDevHandler {
	
private static Logger log = LoggerFactory.getLogger(HelpDevHandler.class);
	
	
	@Execute
	public void execute() {
		log.info((this.getClass().getSimpleName() + " called"));
		Program.launch(Messages.help_dev_link);
		
	}
		
}