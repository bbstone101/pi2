package cn.bbstone.pisces2.ui;

import javax.annotation.PreDestroy;
import javax.inject.Named;

import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MTrimmedWindow;
import org.eclipse.e4.ui.model.application.ui.basic.MWindow;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.e4.ui.workbench.lifecycle.PostContextCreate;
import org.eclipse.e4.ui.workbench.lifecycle.PreSave;
import org.eclipse.e4.ui.workbench.lifecycle.ProcessAdditions;
import org.eclipse.e4.ui.workbench.lifecycle.ProcessRemovals;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;

import au.com.cybersearch2.controls.ControlFactory;
import au.com.cybersearch2.controls.ImageFactory;
import au.com.cybersearch2.statusbar.StatusBar;
import cn.bbstone.pisces2.ui.views.StatusBarView;

public class E4LifeCycle {
	// MTRIMMED_WINDOW_ID must be same as ID of the Application.e4xmi top Trimmed
	// Window
	// "cn.bbstone.pisces2.ui.trimmedwindow.pi2";
	public static final String MTRIMMED_WINDOW_ID = "cn.bbstone.pisces2.ui.trimmedwindow.pi2";// FrameworkUtil.getBundle(E4LifeCycle.class).getSymbolicName();

	ImageFactory imageFactory;
//    ExampleStatusBar exampleStatusBar;
	StatusBarView statusBarView;

//	private Shell shell;
	@PostContextCreate
	void postContextCreate(@Named(IServiceConstants.ACTIVE_SHELL) Shell shell, IEclipseContext workbenchContext) {
//    	this.shell = shell;
		workbenchContext.set(ControlFactory.class, new ControlFactory());
		workbenchContext.set(StatusBar.class, new StatusBar());
		imageFactory = ContextInjectionFactory.make(ImageFactory.class, workbenchContext);
		workbenchContext.set(ImageFactory.class, imageFactory);

		// Install image and SWT widget factories
		imageFactory.setResourceBundle(Activator.getBundle());
//        imageFactory.registerCustomFactory(new PresenceImageFactory());

//        exampleStatusBar = ContextInjectionFactory.make(ExampleStatusBar.class, workbenchContext);
		statusBarView = ContextInjectionFactory.make(StatusBarView.class, workbenchContext);

		// Window static method used to get logo in top presenceItem windows
//        Image[] imageArray = new Image[]{imageFactory.getImage("icons/statusbar/sample.png")};
		Image[] imageArray = new Image[] { imageFactory.getImage("icons/pi/pi32.png") };
		Window.setDefaultImages(imageArray);

	}

	@PreSave
	void preSave(IEclipseContext workbenchContext) {
	}

	@ProcessAdditions
	void processAdditions(MApplication app, EModelService modelService) {
		MTrimmedWindow window = (MTrimmedWindow) modelService.find(MTRIMMED_WINDOW_ID, app);

		// 窗口居中
		Display display = Display.getDefault(); // shell.getDisplay();//Display.getDefault();
		this.setLocation(display, window);
	}

	/**
	 * 窗口居中
	 * 
	 * @param display
	 * @param window
	 */
	private void setLocation(Display display, MWindow window) {
		Monitor monitor = display.getPrimaryMonitor();
		Rectangle monitorRect = monitor.getBounds();
		int x = monitorRect.x + (monitorRect.width - window.getWidth()) / 2;
		int y = monitorRect.y + (monitorRect.height - window.getHeight()) / 2;
		window.setX(x);
		window.setY(y);
	}

	@ProcessRemovals
	void processRemovals(IEclipseContext workbenchContext) {
	}

	@PreDestroy
	void preDestroy() {
		if (imageFactory != null)
			imageFactory.dispose();
		if (statusBarView != null)
			statusBarView.dispose();
	}

}
