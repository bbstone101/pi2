package cn.bbstone.pisces2.ui.views.statusbar;

import org.eclipse.swt.graphics.Image;

import au.com.cybersearch2.controls.ImageFactory;

public enum ConnectionEnum {

	connected(Messages.ConnEnum_connected, "icons/statusbar/link16.png"), //$NON-NLS-2$
	disconnected(Messages.ConnEnum_disconnected, "icons/statusbar/brokenLink16.png");  //$NON-NLS-2$
    
	private String displayText;
	private String imagePath;
	
	ConnectionEnum(String displayText, String imagePath) {
		this.displayText = displayText;
		this.imagePath = imagePath;
	}
	
	public String getDisplayText() {
		return this.displayText;
	}
	
	public Image getMappedImage(ImageFactory imageFactory) {
		return imageFactory.getImage(imagePath);
	}
	
}
