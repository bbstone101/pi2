package cn.bbstone.pisces2.ui.views.statusbar;

import org.eclipse.swt.graphics.Image;

import au.com.cybersearch2.controls.ImageFactory;

/**
 * server/client status
 * 
 * 
 * @author bbstone
 *
 */
public enum PresenceEnum {
	
	online(Messages.PresenceEnum_online, "icons/statusbar/online.gif"),  //$NON-NLS-2$
    loading(Messages.PresenceEnum_loading, "icons/statusbar/loading16.png"), //$NON-NLS-2$
    offline(Messages.PresenceEnum_offline, "icons/statusbar/offline.gif");  //$NON-NLS-2$
    
	private String displayText;
	private String imagePath;
	
	PresenceEnum(String displayText, String imagePath) {
		this.displayText = displayText;
		this.imagePath = imagePath;
	}
	
	public String getDisplayText() {
		return this.displayText;
	}

	public Image getMappedImage(ImageFactory imageFactory) {
		return imageFactory.getImage(imagePath);
	}
	
	
}
