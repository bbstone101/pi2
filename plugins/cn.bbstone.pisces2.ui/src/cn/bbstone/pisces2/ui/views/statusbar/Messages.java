package cn.bbstone.pisces2.ui.views.statusbar;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = Messages.class.getPackageName() + ".messages"; //$NON-NLS-1$
	public static String ConnEnum_connected;
	public static String ConnEnum_disconnected;
	public static String PresenceEnum_loading;
	public static String PresenceEnum_offline;
	public static String PresenceEnum_online;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
