package cn.bbstone.pisces2.ui.views.statusbar;

import org.eclipse.swt.graphics.Image;

import au.com.cybersearch2.controls.ImageFactory;

public enum SecurityEnum {
	
	secure("secure", "icons/statusbar/secure.gif"),
	unsecure("unsecure", "icons/statusbar/unsecure.gif"); 
    
	private String displayText;
	private String imagePath;
	
	SecurityEnum(String displayText, String imagePath) {
		this.displayText = displayText;
		this.imagePath = imagePath;
	}
	
	public String getDisplayText() {
		return this.displayText;
	}
	
	public Image getMappedImage(ImageFactory imageFactory) {
		return imageFactory.getImage(imagePath);
	}
	
}
