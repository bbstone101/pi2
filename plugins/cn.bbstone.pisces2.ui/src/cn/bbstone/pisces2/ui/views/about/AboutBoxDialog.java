
package cn.bbstone.pisces2.ui.views.about;

import org.eclipse.core.runtime.IProduct;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.JFaceColors;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.program.Program;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.osgi.framework.Version;

import cn.bbstone.pisces2.cmm.UIConst;
import cn.bbstone.pisces2.ui.Activator;

/**
 * About box
 */
public class AboutBoxDialog extends MessageDialog {
//	private static final Logger log = LoggerFactory.getLogger(AboutBoxDialog.class);

	public static final String PRODUCT_PROP_SUB_TITLE = "subTitle"; //$NON-NLS-1$
	public static final String PRODUCT_PROP_COPYRIGHT = "copyright"; //$NON-NLS-1$
	public static final String PRODUCT_PROP_WEBSITE = "website"; //$NON-NLS-1$
	public static final String PRODUCT_PROP_EMAIL = "email"; //$NON-NLS-1$

	private final Font NAME_FONT, TITLE_FONT;

	private Image ABOUT_IMAGE;// = new Image(shell.getDisplay(),
								// getClass().getResourceAsStream(UIConst.ICON_CHECKED));
//    AbstractUIPlugin.imageDescriptorFromPlugin(
//        Platform.getProduct().getDefiningBundle().getSymbolicName(),
//        "icons/dbeaver_about.png").createImage();
	private Image splashImage;

	public AboutBoxDialog(Shell shell) {
		// Shell shell, String dialogTitle, Image dialogTitleImage, String
		// dialogMessage, int dialogImageType, String[] dialogButtonLabels, int
		// defaultIndex
		super(shell, Messages.AboutBoxDialog_dialogTitle, null, null, 99,
				new String[] { Messages.AboutBoxDialog_btnOK }, 0);

		NAME_FONT = new Font(shell.getDisplay(), Messages.AboutBoxDialog_dialog_about_font, 14, SWT.BOLD);
		TITLE_FONT = new Font(shell.getDisplay(), Messages.AboutBoxDialog_dialog_about_font, 10, SWT.NORMAL);

		ABOUT_IMAGE = new Image(shell.getDisplay(), getClass().getResourceAsStream("/icons/about/about.png"));
		splashImage = new Image(shell.getDisplay(), getClass().getResourceAsStream("/icons/pi/pi256.png"));
//		splashImage = new Image(shell.getDisplay(), getClass().getResourceAsStream("/icons/about/splash.png"));
	}

	@Override
	public boolean close() {
		NAME_FONT.dispose();
		TITLE_FONT.dispose();

		if (splashImage != null) {
			splashImage.dispose();
			;
		}
		if (ABOUT_IMAGE != null) {
			ABOUT_IMAGE.dispose();
			;
		}
		return super.close();
	}

	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText(Messages.AboutBoxDialog_dialogTitle);
	}

	@Override
	protected boolean isResizable() {
		return true;
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Color background = JFaceColors.getBannerBackground(parent.getDisplay());
		// Color foreground = JFaceColors.getBannerForeground(parent.getDisplay());
		parent.setBackground(background);

		Composite group = new Composite(parent, SWT.NONE);
		group.setBackground(background);
		GridLayout layout = new GridLayout(1, false);
		layout.marginHeight = 20;
		layout.marginWidth = 20;
		group.setLayout(layout);
		group.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		GridData gd;

		IProduct product = Platform.getProduct();

		{
			Label nameLabel = new Label(group, SWT.NONE);
			nameLabel.setBackground(background);
			nameLabel.setFont(NAME_FONT);
			nameLabel.setText(product.getName());
			gd = new GridData(GridData.FILL_HORIZONTAL);
			gd.horizontalAlignment = GridData.CENTER;
			nameLabel.setLayoutData(gd);
		}

		Label titleLabel = new Label(group, SWT.NONE);
		titleLabel.setBackground(background);
		titleLabel.setFont(TITLE_FONT);
		titleLabel.setText(product.getProperty(PRODUCT_PROP_SUB_TITLE));
		gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalAlignment = GridData.CENTER;
		titleLabel.setLayoutData(gd);

		Label imageLabel = new Label(group, SWT.NONE);
		imageLabel.setBackground(background);

		gd = new GridData();
		gd.verticalAlignment = GridData.BEGINNING;
		gd.horizontalAlignment = GridData.CENTER;
		gd.grabExcessHorizontalSpace = false;
		imageLabel.setLayoutData(gd);

		if (splashImage != null) {
			imageLabel.setImage(splashImage);
		} else {
			imageLabel.setImage(ABOUT_IMAGE);
		}

		Text versionLabel = new Text(group, SWT.NONE);
		versionLabel.setEditable(false);
		versionLabel.setBackground(background);
		versionLabel.setText(Messages.AboutBoxDialog_dialog_about_label_version + getProductVersion());
		gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalAlignment = GridData.CENTER;
		versionLabel.setLayoutData(gd);

		Label releaseTimeLabel = new Label(group, SWT.NONE);
		releaseTimeLabel.setBackground(background);
		releaseTimeLabel.setText(Messages.AboutBoxDialog_product_release_date); // $NON-NLS-1$
		gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalAlignment = GridData.CENTER;
		releaseTimeLabel.setLayoutData(gd);

		Label authorLabel = new Label(group, SWT.NONE);
		authorLabel.setBackground(background);
		authorLabel.setText(product.getProperty(PRODUCT_PROP_COPYRIGHT));
		gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalAlignment = GridData.CENTER;
		authorLabel.setLayoutData(gd);

		Link siteLink = createLink(group, makeAnchor(product.getProperty(PRODUCT_PROP_WEBSITE)),
				new SelectionAdapter() {
					@Override
					public void widgetSelected(SelectionEvent e) {
						if (e.text != null && e.text.trim().length() > 0)
							Program.launch(e.text);
					}
				});
		siteLink.setBackground(background);
		gd = new GridData();
		gd.horizontalAlignment = GridData.CENTER;
		siteLink.setLayoutData(gd);
//
//		String infoDetails = DBWorkbench.getPlatform().getApplication().getInfoDetails(new VoidProgressMonitor());
//		if (!CommonUtils.isEmpty(infoDetails)) {
//			Text extraText = new Text(group, SWT.MULTI | SWT.BORDER | SWT.READ_ONLY | SWT.WRAP | SWT.V_SCROLL);
//			extraText.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
//			extraText.setText(infoDetails);
//		}

		return parent;
	}

	public static Link createLink(Composite parent, String text, SelectionListener listener) {
		Link link = new Link(parent, SWT.NONE);
		link.setText(text);
		link.addSelectionListener(listener);
		return link;
	}

	public static String makeAnchor(String text) {
		return "<a>" + text + "</a>"; //$NON-NLS-1$ //$NON-NLS-2$
	}
	
	private String getProductVersion() {
		Version version = null;
		final IProduct product = Platform.getProduct();
        if (product == null) {
        	version = Activator.getBundle().getVersion();
        } else {
        	version =  product.getDefiningBundle().getVersion();
        }
        return (version != null) ? version.toString() : UIConst.EMPTY_STR;
	}

}