package cn.bbstone.pisces2.ui.views;

import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import cn.bbstone.pisces2.model.Config;

/**
 * after configuration changes, pop-up dialog confirm by user
 * 
 * @author bbstone
 *
 */
public class ConfigTitleAreaDialog extends TitleAreaDialog {

	
	private Config config;
	private Text txtPort;
	private Text txtHost;

//	private String firstName;
//	private String lastName;
	private Text txtFileFilter;
	private Text txtRootPath;
	private Composite container_1;
	private Label lblPort;
	private Label lblHost;

	public ConfigTitleAreaDialog(Shell parentShell) {
        super(parentShell);
        setShellStyle(SWT.CLOSE);
    }

	@Override
	public void create() {
		super.create();
		setTitle("服务器设置");
		setMessage("设置项包括：服务器运行的IP，端口，文件扫描的根路径和文件过滤规则。", IMessageProvider.INFORMATION);
	}
	
	public void initFormData(Config config) {
		txtPort.setText(String.valueOf(config.getPort()));
		txtHost.setText(config.getHost());
		txtFileFilter.setText(config.getFilter());
		txtRootPath.setText(config.getRootPath());
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite area = (Composite) super.createDialogArea(parent);
		container_1 = new Composite(area, SWT.NONE);
		container_1.setLayout(new FormLayout());
		container_1.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		createFirstName(container_1);
		createLastName(container_1);
		
		Label lblFileFilter = new Label(container_1, SWT.NONE);
		FormData fd_lblFileFilter = new FormData();
		fd_lblFileFilter.top = new FormAttachment(0, 54);
		fd_lblFileFilter.left = new FormAttachment(0, 15);
		lblFileFilter.setLayoutData(fd_lblFileFilter);
		lblFileFilter.setText("过滤器：");
		
		txtFileFilter = new Text(container_1, SWT.BORDER);
		FormData fd_txtFileFilter = new FormData();
		fd_txtFileFilter.right = new FormAttachment(0, 493);
		fd_txtFileFilter.top = new FormAttachment(0, 53);
		fd_txtFileFilter.left = new FormAttachment(0, 68);
		txtFileFilter.setLayoutData(fd_txtFileFilter);
		
		Label lblRootPath = new Label(container_1, SWT.NONE);
		FormData fd_lblRootPath = new FormData();
		fd_lblRootPath.top = new FormAttachment(0, 78);
		fd_lblRootPath.left = new FormAttachment(0, 15);
		lblRootPath.setLayoutData(fd_lblRootPath);
		lblRootPath.setText("根路径：");
		
		txtRootPath = new Text(container_1, SWT.BORDER);
		FormData fd_txtRootPath = new FormData();
		fd_txtRootPath.right = new FormAttachment(0, 493);
		fd_txtRootPath.top = new FormAttachment(0, 77);
		fd_txtRootPath.left = new FormAttachment(0, 68);
		txtRootPath.setLayoutData(fd_txtRootPath);

		return area;
	}

	private void createFirstName(Composite container) {
		lblPort = new Label(container, SWT.NONE);
		FormData fd_lblPort = new FormData();
		fd_lblPort.top = new FormAttachment(0, 6);
		fd_lblPort.left = new FormAttachment(0, 15);
		lblPort.setLayoutData(fd_lblPort);
		lblPort.setText("端口：");

		txtPort = new Text(container, SWT.BORDER);
		FormData fd_txtPort = new FormData();
		fd_txtPort.top = new FormAttachment(0, 5);
		fd_txtPort.left = new FormAttachment(lblPort, 17);
		fd_txtPort.right = new FormAttachment(0, 123);
		txtPort.setLayoutData(fd_txtPort);
		txtPort.setTextLimit(5);
	}

	private void createLastName(Composite container) {
		lblHost = new Label(container, SWT.NONE);
		FormData fd_lblHost = new FormData();
		fd_lblHost.bottom = new FormAttachment(0, 48);
		fd_lblHost.top = new FormAttachment(0, 29);
		fd_lblHost.left = new FormAttachment(0, 15);
		lblHost.setLayoutData(fd_lblHost);
		lblHost.setText("服务器：");
		txtHost = new Text(container, SWT.BORDER);
		FormData fd_txtHost = new FormData();
		fd_txtHost.top = new FormAttachment(txtPort, 5);
		fd_txtHost.left = new FormAttachment(lblHost, 5);
		fd_txtHost.right = new FormAttachment(0, 237);
		txtHost.setLayoutData(fd_txtHost);
		txtHost.setTextLimit(30);
	}

	@Override
	protected boolean isResizable() {
		return true;
	}

	// save content of the Text fields because they get disposed
	// as soon as the Dialog closes
	private void saveInput() {
		config = new Config();
		config.setPort(Integer.valueOf(txtPort.getText()));
		config.setHost(txtHost.getText());
		config.setFilter(txtFileFilter.getText());
		config.setRootPath(txtRootPath.getText());
		
//		firstName = txtPort.getText();
//		lastName = txtHost.getText();
		
	}

	@Override
	protected void okPressed() {
		saveInput();
		super.okPressed();
	}

	public Config getConfig() {
		return config;
	}
	

//	public String getFirstName() {
//		return firstName;
//	}
//
//	public String getLastName() {
//		return lastName;
//	}
}
