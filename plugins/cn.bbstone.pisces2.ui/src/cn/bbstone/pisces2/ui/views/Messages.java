package cn.bbstone.pisces2.ui.views;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = Messages.class.getPackageName() + ".messages"; //$NON-NLS-1$
	public static String FileListView_search;
	public static String FileListView_tb_fileCat;
	public static String FileListView_tb_isDelivery;
	public static String FileListView_tb_rpath;
	public static String FileListView_tb_seqNo;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
