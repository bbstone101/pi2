package cn.bbstone.pisces2.ui.views.about;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = Messages.class.getPackageName() + ".messages"; //$NON-NLS-1$
	public static String AboutBoxDialog_btnOK;
	public static String AboutBoxDialog_dialogTitle;
	public static String AboutBoxDialog_dialog_about_font;
	public static String AboutBoxDialog_dialog_about_label_version;
	public static String AboutBoxDialog_product_release_date;
	
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
