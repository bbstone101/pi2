//package cn.bbstone.pisces2.ui.views;
//
//import javax.annotation.PostConstruct;
//import javax.annotation.PreDestroy;
//import javax.inject.Inject;
//
//import org.eclipse.core.resources.ResourcesPlugin;
//import org.eclipse.core.runtime.IProgressMonitor;
//import org.eclipse.core.runtime.IStatus;
//import org.eclipse.core.runtime.NullProgressMonitor;
//import org.eclipse.core.runtime.Status;
//import org.eclipse.core.runtime.SubMonitor;
//import org.eclipse.core.runtime.jobs.IJobChangeEvent;
//import org.eclipse.core.runtime.jobs.Job;
//import org.eclipse.core.runtime.jobs.JobChangeAdapter;
//import org.eclipse.core.runtime.jobs.ProgressProvider;
//import org.eclipse.e4.ui.di.UISynchronize;
//import org.eclipse.e4.ui.model.application.MApplication;
//import org.eclipse.e4.ui.model.application.ui.menu.MToolControl;
//import org.eclipse.e4.ui.workbench.modeling.EModelService;
//import org.eclipse.jface.dialogs.MessageDialog;
//import org.eclipse.swt.SWT;
//import org.eclipse.swt.widgets.Composite;
//import org.eclipse.swt.widgets.ProgressBar;
//import org.eclipse.swt.widgets.Shell;
//
//import lombok.extern.slf4j.Slf4j;
//
//@Slf4j
//public class ProgressStatusBar {
//
//	private ProgressTextToolControl progressTextToolControl;
//
////	@Inject
//	private EModelService modelService;
////	@Inject
//	private MApplication application;
//
//	@Inject
//	private UISynchronize sync;
//
//	private ProgressBar progressBar;
//
//	@Inject
//	private Shell shell;
//
//	@Inject
//	public ProgressStatusBar(EModelService modelService, MApplication application) {
////		this.sync = Objects.requireNonNull(sync);
//		this.modelService = modelService;
//		this.application = application;
////		MToolControl toolControll = (MToolControl) modelService.find("cn.bbstone.pisces2.ui.toolcontrol.progress.text", application);
////		progressTextToolControll = (ProgressTextToolControll)toolControll.getObject();
//	}
//
//	@PostConstruct
//	public void createGui(Composite parent) {
//		progressBar = new ProgressBar(parent, SWT.SMOOTH);
//		progressBar.setBounds(100, 10, 200, 20);
//
//		Job.getJobManager().setProgressProvider(new ProgressProvider() {
//			@Override
//			public IProgressMonitor createMonitor(Job job) {
//				StatusUpdateProgressMonitor statusUpdateProgressMonitor = new StatusUpdateProgressMonitor();
//				return statusUpdateProgressMonitor.addJob(job);
//			}
//		});
//
//		Job job = new Job("Load File List") {
//			@Override
//			protected IStatus run(IProgressMonitor monitor) {
//				monitor.beginTask("other tasks", 100);
//				monitor.worked(30);
////				SubMonitor subMonitor = SubMonitor.convert(monitor, 100);
////				subMonitor.setWorkRemaining(70);
//				// time consuming work here
//				doExpensiveWork(monitor);
//				// sync with UI
////				syncWithUI();
//				return Status.OK_STATUS;
//			}
//		};
//		// if set rule, will used progress bar exclusive
//		job.setRule(ResourcesPlugin.getWorkspace().getRoot());
////		job.setUser(true);
////		job.setSystem(true);
//		job.schedule();
//
//	}
//
//	private void doExpensiveWork(IProgressMonitor monitor) {
//		// mimic a long time job here
//		for (int i = 0; i < 10; i++) {
//			try {
//				// give a progress bar to indicate progress
//				int rate =  (int) ((1/10f) * 70);
//				monitor.worked(rate);
//				
//				log.info("step: " + (i + 1));
//
//				// 
//				MToolControl toolControll = (MToolControl) modelService.find("cn.bbstone.pisces2.ui.toolcontrol.progress.text", application);
//				progressTextToolControl = (ProgressTextToolControl)toolControll.getObject();
//				int percent100 = (int)(((i + 1) / 10f) * 70) + 30;
//				sync.asyncExec(new Runnable() {
//					public void run() {
//						progressTextToolControl.setProgressText("progress: " + percent100 + "%");
//						log.info("current progress:  {} %", percent100);
//					}
//				});
//				
//				Thread.sleep(500);
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}
//		}
//	}
//
//	private void syncWithUI() {
////		Display.getDefault().asyncExec(new Runnable() {
//		sync.asyncExec(new Runnable() {
//			public void run() {
//				MessageDialog.openInformation(shell, "message", "completed!");
//			}
//		});
//	}
//
//	@PreDestroy
//	public void destroy() {
////		while (Job.getJobManager().currentJob() != null) {
////			Job.getJobManager().currentJob().cancel();
////		}
////		if (progressBar != null)
////			progressBar.dispose();
//
//	}
//
//	private final class StatusUpdateProgressMonitor extends NullProgressMonitor {
//		// thread-Safe via thread confinement of the UI-Thread
//		// (means access only via UI-Thread)
//		private long runningTasks = 0L;
//
//		@Override
//		public void beginTask(final String name, final int totalWork) {
//			sync.syncExec(new Runnable() {
//				@Override
//				public void run() {
//					if (runningTasks <= 0) {
//						// --- no task is running at the moment ---
//						progressBar.setSelection(0);
//						progressBar.setMaximum(totalWork);
//					} else {
//						// --- other tasks are running ---
//						progressBar.setMaximum(progressBar.getMaximum() + totalWork);
//					}
//					runningTasks++;
//					progressBar.setToolTipText("Currently running: " + runningTasks + "\nLast task: " + name);
////					log.info("Currently running: {} , Last task: {}", runningTasks, name);
//				}
//			});
//		}
//
//		@Override
//		public void worked(final int work) {
//			sync.syncExec(new Runnable() {
//				@Override
//				public void run() {
//					progressBar.setSelection(progressBar.getSelection() + work);
//				}
//			});
//		}
//
//		public IProgressMonitor addJob(Job job) {
//			if (job != null) {
//				job.addJobChangeListener(new JobChangeAdapter() {
//					@Override
//					public void done(IJobChangeEvent event) {
//						if (shell.isDisposed()) return;
//						sync.syncExec(new Runnable() {
//							@Override
//							public void run() {
//								if (progressBar.isDisposed()) return;
//								runningTasks--;
//								if (runningTasks > 0) {
//									// --- some tasks are still running ---
//									progressBar.setToolTipText("Currently running: " + runningTasks);
//								} else {
//									// --- all tasks are done (a reset of selection could also be done) ---
//									progressBar.setToolTipText("No background progress running.");
//								}
//							}
//						});
//						// clean-up
//						event.getJob().removeJobChangeListener(this);
//					}
//				});
//			}
//			return this;
//		}
//	}
//
//}