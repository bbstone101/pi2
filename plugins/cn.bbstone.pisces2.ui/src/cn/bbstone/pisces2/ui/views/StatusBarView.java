package cn.bbstone.pisces2.ui.views;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.e4.ui.di.UISynchronize;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;

import au.com.cybersearch2.controls.ControlFactory;
import au.com.cybersearch2.controls.ImageFactory;
import au.com.cybersearch2.statusbar.StatusBar;
import au.com.cybersearch2.statusbar.StatusItem;
import au.com.cybersearch2.statusbar.controls.ItemConfiguration;
import cn.bbstone.pisces2.ui.base.notify.Pi2Events;
import cn.bbstone.pisces2.ui.views.statusbar.ConnectionEnum;
import cn.bbstone.pisces2.ui.views.statusbar.PresenceEnum;
import cn.bbstone.pisces2.ui.views.statusbar.SecurityEnum;

/**
 * StatusBar Status line control built on Statusbar plugin.
 * <p>
 * The Status bar consists of a StatusLine control, which is
 * securityItem-aligned with the main window and zero or more
 * StatusLineContribution controls which are arranged to fill the remaining
 * Status bar space. The last control to be contributed is securityItem-aligned
 * with the main window. If there is more than one contribution, then the second
 * last one is expaned to fill the remaining space. Having more than 3 controls
 * in the Status bar is not recommended, but there is really no limit on how
 * many may be inserted.
 * </p>
 * 
 * @author
 */
public class StatusBarView {

	/**
	 * All StatusBar controls are defined using a
	 * {@link au.com.cybersearch2.statusbar.controls.ItemConfiguration#} object
	 * which has a "hintWidth" value. This parameter is interpreted as the width in
	 * number of characters. If set to zero, then then the extent of the control is
	 * evaluated using the text to be displayed. The following value is assigned as
	 * the width of the first control, which may display "Do not disturb", which has
	 * 14 characters.
	 */
	private final static int LEFT_CHAR_WIDTH = 18;

	static ItemConfiguration OPERATION_CONFIGURATION;
	static ItemConfiguration CONNECTION_CONFIGURATION;

	private static final String DISCONNECTED = "Disconnected";

	static {
		OPERATION_CONFIGURATION = new ItemConfiguration(null, "Open main window.", 0);
		CONNECTION_CONFIGURATION = new ItemConfiguration(null, DISCONNECTED, DISCONNECTED.length());
	}

	/** First item aligned to presenceItem of client window area */
	StatusItem presenceItem;

	/**
	 * Second of 3 items aligned with presenceItem item and expands to fill space up
	 * to securityItem item
	 */
	StatusItem connectionItem;

	/** Third of 3 items securityItem-aligned with client window area */
	StatusItem securityItem;

	/**
	 * Reserve item used to display message while connectionItem is used for menu
	 * demo
	 */
	StatusItem operationItem;

	@Inject
	StatusBar statusBar;

	/** Image factory and resource manager */
	@Inject
	ImageFactory imageFactory;

	/** SWT widget factory */
	@Inject
	ControlFactory controlFactory;

	/** Synchronizes current thread while executing in the UI-Thread */
	@Inject
	UISynchronize sync;

	/**
	 * postConstruct
	 */
	@PostConstruct
	void postConstruct() {
//		CONNECTION_CONFIGURATION.setImage(imageFactory.getImage("icons/statusbar/blank.gif"));

		presenceItem = new StatusItem(new ItemConfiguration(PresenceEnum.offline.getMappedImage(imageFactory), "Offline", LEFT_CHAR_WIDTH), 0);
		connectionItem = new StatusItem(OPERATION_CONFIGURATION, 1);
		securityItem = new StatusItem(new ItemConfiguration(null, "Security Status", 0), 3);

		statusBar.addStatusItem(presenceItem);
		statusBar.addStatusItem(connectionItem);
		statusBar.addStatusItem(securityItem);
	}

	/**
	 * Handle presenceItem updated event
	 * 
	 * @param presenceItem The new presenceItem value
	 */
	@Inject
	@Optional
	void onPresence(final @UIEventTopic(Pi2Events.PRESENCE) PresenceEnum presenceEnum) {
		presenceItem.setLabel(presenceEnum.getDisplayText(), presenceEnum.getMappedImage(imageFactory));
	}

	@Inject
	@Optional
	void onConnect(final @UIEventTopic(Pi2Events.CONNECTION) ConnectionEnum connectionEnum) {
		connectionItem.setLabel(connectionEnum.getDisplayText(), connectionEnum.getMappedImage(imageFactory));
//		if (!operationText.isEmpty())
//			connectionItem.update(new ItemConfiguration(null, operationText, 0));
//		else
//			connectionItem.update(OPERATION_CONFIGURATION);
	}


	/**
	 * Handle user message modified event
	 * 
	 * @param message The new message to display
	 */
	@Inject
	@Optional
	void onOperate(final @UIEventTopic(Pi2Events.OPERATION) String message) {
		if (operationItem == null) {
			ItemConfiguration messageConfig = new ItemConfiguration(null, message, 0);
			operationItem = new StatusItem(messageConfig, 2);
			statusBar.addStatusItem(operationItem);
		} else {
			operationItem.setText(message);
		}
	}

	/**
	 * Handle secure status updated
	 * 
	 * @param isSecure Flag set true if secure status achieved
	 */
	@Inject
	@Optional
	void onSecure(final @UIEventTopic(Pi2Events.SECURITY) SecurityEnum securityEnum) {
		Image image = securityEnum.getMappedImage(imageFactory);
		if (!securityItem.getText().isEmpty())
			// Remove text as this is an image-only control
			securityItem.update(new ItemConfiguration(image, "", SWT.DEFAULT));
		else
			securityItem.setImage(image);
	}

	
//	public static class Events {
//		/** Server/Client Status: online, offline, loading */
//		public static final String PRESENCE = "statusbar/presence";
//
//		/** server-client connected or not: connected, disconnected */
//		public static final String CONNECTION = "statusbar/connection";
//
//		/** User message update */
//		public static final String OPERATION = "statusbar/operation";
//
//		/** Messaging secure/unsecure */
//		public static final String SECURITY = "statusbar/security";
//		
//	}
	
	public void dispose() {
		if (statusBar != null) {
			statusBar.dispose();
		}
	}

}
