package cn.bbstone.pisces2.ui;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

import cn.bbstone.pisces2.cmm.Pi2ContextUtil;
import cn.bbstone.pisces2.ui.preference.DefaultValuesInitializer;

public class Activator implements BundleActivator {

//	@Inject
//	private E4LogbackAppender<ILoggingEvent> e4LogbackAppender;
	
	private static BundleContext context;
	
//	private static LoggerContext loggerContext;

	/** Bundle in BundleContext of most recent start() call */
	private static Bundle bundle;

	static BundleContext getContext() {
		return context;
	}

//	private static final String configFile = "conf/config.properties";
//	public static Properties properties = new Properties();

	@Override
	public void start(BundleContext bundleContext) throws Exception {
		setContext(bundleContext);
		setBundle(bundleContext.getBundle());
		
		// reset context
		Pi2ContextUtil.reset();
		
		// load default configuration to preference
		DefaultValuesInitializer.loadDefaultValues();
		
//		configureLogbackInBundle(bundleContext.getBundle());
		
	}

	@Override
	public void stop(BundleContext bundleContext) throws Exception {
		Activator.context = null;
	}

	/**
	 * Set bundle context singleton. Keeps Findbugs happy when set by method call
	 * from non-static class member.
	 * 
	 * @return BundleContext
	 */
	static void setContext(BundleContext bundleContext) {
		context = bundleContext;
	}

	/**
	 * Set bundle singleton. Keeps Findbugs happy when set by method call from
	 * non-static class member.
	 * 
	 * @return Bundle
	 */
	static void setBundle(Bundle value) {
		bundle = value;
	}

	public static Bundle getBundle() {
		return bundle;
	}

	public static BundleContext getBundleContext() {
		return context;
	}
	
	
}
