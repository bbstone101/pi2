package cn.bbstone.pisces2.ui.preference;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = Messages.class.getPackageName() + ".messages"; //$NON-NLS-1$
	public static String basic_preference_clientNum;
	public static String basic_preference_clientRoot;
	public static String basic_preference_fileFilter;
	public static String basic_preference_isOverwritten;
	public static String basic_preference_recvBuff;
	public static String basic_preference_runAs;
	public static String basic_preference_runAsClient;
	public static String basic_preference_runAsServer;
	public static String basic_preference_serverHost;
	public static String basic_preference_serverPort;
	public static String basic_preference_serverRoot;
	public static String basic_preference_tempPostfix;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
