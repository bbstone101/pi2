package cn.bbstone.pisces2.ui.preference;

public enum FieldInfoEnum {

	
	/** Pi2 run as server or client mode */
	RUN_AS("run_as", "运行模式: "),
	RUN_AS_SERVER("server", "服务器模式: "),
	RUN_AS_CLIENT("client", "客户端模式: "),
	
	// -------------------- common configurations(both required by server and client) 
	/** FLI(File List Index) filter, matched files will not load in fli.idx file  */
	FLI_FILTER("fli_filter", "文件过滤器: "),
	SERVER_PORT("server_port", "服务器端口: "),
	SERVER_HOST("server_host", "服务器主机"),
	
	// -------------------- server configurations fields only 
	SERVER_ROOT("server_root_path", "服务器根路径: "),
	
	// -------------------- client configurations fields only
	/** transferring file postfix */
	CLIENT_FILE_TEMP_POSTFIX("client_file_temp_postfix", "临时文件后缀: "),
	/**
	 * overwrite file exits in client
	 * 	false - never overwrite
	 * 	true - overwrite client file with server fileData only when client file checksum diff from server file checksum
	 */
	CLIENT_FILE_OVERWRITE("client_file_overwrite", "覆盖存在文件: "),
	/**
	 * how many clients connect to server
	 */
	CLIENT_CONN_NUM("client_conn_num", "客户端的数量: "),
	/**
	 * FileTask receive file data buffer size
	 * if there are logs small files(single file.size < 4M), tune it down, e.g. 4M
	 * if there are logs big files(single file.size > 3G), tune it up, e.g. 32M
	 */
	CLIENT_RECV_BUFF("client_recv_buff", "文件接收缓存: "),
	/** */
	CLIENT_ROOT("client_root_path", "客户端根路径: "),
	
	// ------------------- others reserved
	other("X", "Y");
	
	private String preferenceName;
	private String labelText;
	
	FieldInfoEnum(String preferenceName, String labelText) {
		this.preferenceName = preferenceName;
		this.labelText = labelText;
	}

	public String getPreferenceName() {
		return preferenceName;
	}

	public String getLabelText() {
		return labelText;
	}
	
	
	
}
