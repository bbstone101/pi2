package cn.bbstone.pisces2.ui.preference;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.IEclipsePreferences.PreferenceChangeEvent;
import org.osgi.service.prefs.BackingStoreException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.bbstone.pisces2.cmm.Pi2ContextUtil;
import cn.bbstone.pisces2.cmm.UIConst;
import cn.bbstone.pisces2.comm.Const;
import cn.bbstone.pisces2.config.ConfigFactory;
import cn.bbstone.pisces2.config.ConfigModel;
import cn.bbstone.pisces2.ui.base.util.PreferenceUtil;
import cn.bbstone.pisces2.util.NetUtil;

public class DefaultValuesInitializer extends AbstractPreferenceInitializer {

//	private static final boolean win = Platform.getOS().equals(Platform.OS_WIN32);
//    private static final boolean mac = Platform.getOS().equals(Platform.OS_MACOSX);
//    private static final boolean lin = Platform.getOS().equals(Platform.OS_LINUX);

	private static Logger log = LoggerFactory.getLogger(DefaultValuesInitializer.class);

//	public static final String NODE_PATH = FrameworkUtil.getBundle(DefaultValuesInitializer.class).getSymbolicName(); // "cn.bbstone.pisces2.ui"; // 

	public DefaultValuesInitializer() {
		log.info("DefaultValuesInitializer constructed.");
	}

	@Override
	public void initializeDefaultPreferences() {
		// cn.bbstone.pisces2.ui
//		String symbolicName = FrameworkUtil.getBundle(getClass()).getSymbolicName();
//		System.out.println("symbolicName: " + NODE_PATH);
//		log.info("symbolicName: {}", NODE_PATH);

		loadDefaultValues();
	}

	public static void loadDefaultValues() {
//		IEclipsePreferences preferences = InstanceScope.INSTANCE.getNode(DefaultValuesInitializer.NODE_PATH);
//		IEclipsePreferences preferences = DefaultScope.INSTANCE.getNode(NODE_PATH);

		// InstanceScope click "Restore Defaults" button not worked
		IEclipsePreferences preferences = PreferenceUtil.getPreference(); // .INSTANCE.getNode(PreferenceUtil.NODE_ID);
		// DefaultScope "Restore Defaults" button work fine.
//		IEclipsePreferences preferences = DefaultScope.INSTANCE.getNode(FrameworkUtil.getBundle(DefaultValuesInitializer.class).getSymbolicName());
		if (preferences != null) {
			// NOTICE!!!: DefaultScope will not trigger PreferenceChangeEvent
			preferences.addPreferenceChangeListener(new IEclipsePreferences.IPreferenceChangeListener() {
				@Override
				public void preferenceChange(PreferenceChangeEvent event) {
					if ("port".equals(event.getKey())) {
//						System.out.println("Key: port, Value: old: " + event.getOldValue() + ", new: " + event.getNewValue());
						log.info("Key: port, Value: old: {} , new: {}", event.getOldValue(), event.getNewValue());
					}
				}
			});

			String runAs = preferences.get(FieldInfoEnum.RUN_AS.getPreferenceName(), UIConst.RUN_AS_SERVER);
			Pi2ContextUtil.setRunAs(runAs); // NOTICE, after preferences update, need check for update again

			ConfigModel serverConfigModel = ConfigFactory.loadConfigs(Const.SERVER);
			ConfigModel clientConfigModel = ConfigFactory.loadConfigs(Const.CLIENT);
			Pi2ContextUtil.setServerConfigModel(serverConfigModel);
			Pi2ContextUtil.setClientConfigModel(clientConfigModel);

			// common field value initial
			preferences.put(FieldInfoEnum.RUN_AS.getPreferenceName(), runAs); // default Pi2 run as server

			String filter = serverConfigModel.getFilter();
			preferences.put(FieldInfoEnum.FLI_FILTER.getPreferenceName(), filter); // ".DS_Store,.mvn,.git");

			int port = serverConfigModel.getServerPort();
			preferences.putInt(FieldInfoEnum.SERVER_PORT.getPreferenceName(), port); // "8899");

//			String host = serverConfigModel.getServerHost();
			String host = NetUtil.getLocalHostAddress();
			preferences.put(FieldInfoEnum.SERVER_HOST.getPreferenceName(), host); // "localhost");

			if (UIConst.RUN_AS_SERVER.equals(runAs)) {
				String serverRoot = serverConfigModel.getServerRoot();
				preferences.put(FieldInfoEnum.SERVER_ROOT.getPreferenceName(), serverRoot); // "/Users/bbstone/workdir/assets");
			}

			String postfix = clientConfigModel.getPostfix();
			preferences.put(FieldInfoEnum.CLIENT_FILE_TEMP_POSTFIX.getPreferenceName(), postfix);

			boolean isOverwrite = clientConfigModel.isOverwrite();
			preferences.putBoolean(FieldInfoEnum.CLIENT_FILE_OVERWRITE.getPreferenceName(), isOverwrite);

			int connNum = clientConfigModel.getConnNum();
			preferences.putInt(FieldInfoEnum.CLIENT_CONN_NUM.getPreferenceName(), connNum);

			String recvBuff = clientConfigModel.getRecvBuffer();
			preferences.put(FieldInfoEnum.CLIENT_RECV_BUFF.getPreferenceName(), recvBuff);

			String clientRoot = clientConfigModel.getClientRoot();
			preferences.put(FieldInfoEnum.CLIENT_ROOT.getPreferenceName(), clientRoot);

			try {
				preferences.flush();
			} catch (BackingStoreException e) {
				log.error("flush preference error.", e);
			}

		}
	}
	

}
