package cn.bbstone.pisces2.ui.parts;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = Messages.class.getPackageName() + ".messages"; //$NON-NLS-1$
	public static String ConnInfoPart_clients;
	public static String ConnInfoPart_serverHost;
	public static String ConnInfoPart_serverPort;
	public static String SavePointPart_checksum;
	public static String SavePointPart_count;
	public static String SavePointPart_fileNo;
	public static String SavePointPart_updateTime;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
