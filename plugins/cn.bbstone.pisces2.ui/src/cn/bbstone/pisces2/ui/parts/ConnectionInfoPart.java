package cn.bbstone.pisces2.ui.parts;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;

import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.di.Focus;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Text;

import cn.bbstone.pisces2.cmm.UIConst;
import cn.bbstone.pisces2.config.ConfigModel;
import cn.bbstone.pisces2.listener.OpEnum;
import cn.bbstone.pisces2.ui.base.enums.CmdEnum;
import cn.bbstone.pisces2.ui.base.notify.Pi2Events;
import cn.bbstone.pisces2.ui.views.statusbar.ConnectionEnum;
import cn.bbstone.pisces2.ui.views.statusbar.PresenceEnum;
import cn.bbstone.pisces2.ui.views.statusbar.SecurityEnum;
import cn.bbstone.pisces2.util.CtxUtil;

public class ConnectionInfoPart {
//	private static Logger log = LoggerFactory.getLogger(ConnectionInfoPart.class);
	
	@Inject
	IEventBroker eventBroker;
	
	private Text txtServerPort;
	private Text txtServerHost;
	private List listConnClients;
	
	@PostConstruct
	public void createControls(Composite parent) {
		parent.setLayout(new GridLayout(2, false));
		
		Label lblServerPort = new Label(parent, SWT.NONE);
		lblServerPort.setAlignment(SWT.RIGHT);
		lblServerPort.setText(Messages.ConnInfoPart_serverPort);
		
		txtServerPort = new Text(parent, SWT.BORDER);
		GridData gd_txtServerPort = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_txtServerPort.widthHint = 159;
		txtServerPort.setLayoutData(gd_txtServerPort);
		
		Label lblServerHost = new Label(parent, SWT.NONE);
		lblServerHost.setAlignment(SWT.RIGHT);
		lblServerHost.setText(Messages.ConnInfoPart_serverHost);
		
		txtServerHost = new Text(parent, SWT.BORDER);
		GridData gd_txtServerHost = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_txtServerHost.widthHint = 164;
		txtServerHost.setLayoutData(gd_txtServerHost);
		
		Label lblConnClients = new Label(parent, SWT.NONE);
		lblConnClients.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		lblConnClients.setAlignment(SWT.RIGHT);
		lblConnClients.setText(Messages.ConnInfoPart_clients);
		
		listConnClients = new List(parent, SWT.BORDER);
		GridData gd_listConnClients = new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1);
		gd_listConnClients.widthHint = 165;
		gd_listConnClients.heightHint = 217;
		listConnClients.setLayoutData(gd_listConnClients);
		// presenceItem
		eventBroker.post(Pi2Events.PRESENCE, PresenceEnum.offline);
		// connectionItem
		eventBroker.post(Pi2Events.CONNECTION, ConnectionEnum.disconnected);
		// operationItem
		eventBroker.post(Pi2Events.OPERATION, OpEnum.s1_initial.getDisplayText());
		// securityItem
		eventBroker.post(Pi2Events.SECURITY, SecurityEnum.unsecure);
		
	}
	
	@Focus
	public void setFocus() {
//		text.setFocus();
	}

	@PreDestroy
	public void dispose() {
		
	}
	
	
	/** 
	 * only server trigger this event
	 * 
	 * @param cmdEnum
	 */
	@Inject
	@Optional
	void onClientConnected(final @UIEventTopic(Pi2Events.CLIENT_CONNECTED_CONNINFO) String clientAddr) {
//		ConnInfo connInfo = CtxUtil.getConnInfo();
		ConfigModel configModel =  CtxUtil.getConfigModel();
		updateFormData(configModel, clientAddr);
	}
	
	@Inject
	@Optional
	void onClientDisConnected(final @UIEventTopic(Pi2Events.CLIENT_DISCONNECTED_CONNINFO) String clientAddr) {
		removeClientAddr(clientAddr);
	}
	
	@Inject
	@Optional
	void onServerShutdown(final @UIEventTopic(Pi2Events.SERVER_SHUTDOWN_CONNINFO) String message) {
		clearFormData();
	}
	
	@Inject
	@Optional
	void onClientShutdown(final @UIEventTopic(Pi2Events.CLIENT_SHUTDOWN_CONNINFO) String message) {
		clearFormData();
	}
	
	@Inject
	@Optional
	void onServerDisconnected(final @UIEventTopic(Pi2Events.SERVER_DISCONNECTED_CONNINFO) CmdEnum cmdEnum) {
		clearFormData();
	}
	
	private synchronized void updateFormData(ConfigModel configModel, String clientAddr) {
		txtServerPort.setText(String.valueOf(configModel.getServerPort()));
		txtServerHost.setText(configModel.getServerHost());
		listConnClients.add(clientAddr);
//		listConnClients.removeAll();
//		while (connInfo.getClientAddrIterator().hasNext()) {
//			String clientAddr = connInfo.getClientAddrIterator().next();
//			listConnClients.add(clientAddr);
//		}
	}
	
	private void removeClientAddr(String clientAddr) {
//		txtServerPort.setText(UIConst.EMPTY_STR);
//		txtServerHost.setText(UIConst.EMPTY_STR);
		listConnClients.remove(clientAddr);
//		listConnClients.removeAll();
	}
	
	private void clearFormData() {
		txtServerPort.setText(UIConst.EMPTY_STR);
		txtServerHost.setText(UIConst.EMPTY_STR);
//		listConnClients.removeAll();
	}
	
	
}
