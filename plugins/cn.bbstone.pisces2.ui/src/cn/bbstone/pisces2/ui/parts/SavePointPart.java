package cn.bbstone.pisces2.ui.parts;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
//import org.eclipse.wb.swt.SWTResourceManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.bbstone.pisces2.cmm.Pi2ContextUtil;
import cn.bbstone.pisces2.cmm.RunModeEnum;
import cn.bbstone.pisces2.cmm.UIConst;
import cn.bbstone.pisces2.listener.OpEnum;
import cn.bbstone.pisces2.model.Savepoint;
import cn.bbstone.pisces2.service.SavepointService;
import cn.bbstone.pisces2.ui.base.SWTResourceManager;
import cn.bbstone.pisces2.ui.base.enums.CmdEnum;
import cn.bbstone.pisces2.ui.base.notify.Pi2Events;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;

public class SavePointPart {
	private static Logger log = LoggerFactory.getLogger(SavePointPart.class);

	private Text txtChecksum;
	private Text txtCount;
	private Text txtFileNo;
	private Text txtUpdateTime;

	@Inject
	IEventBroker eventBroker;

	@Inject
	private SavepointService savepointService;

	public SavePointPart() {
//		System.out.println(this.getClass().getSimpleName() + " constructed");
		log.info((this.getClass().getSimpleName() + " constructed")); //$NON-NLS-1$
	}

	@PostConstruct
	public void createControls(Composite parent) {
		parent.setLayout(new GridLayout(2, false));

		Label lblFliidx = new Label(parent, SWT.NONE);
		lblFliidx.setAlignment(SWT.RIGHT);
		lblFliidx.setText(Messages.SavePointPart_checksum);

		txtChecksum = new Text(parent, SWT.BORDER | SWT.READ_ONLY);
		GridData gd_txtChecksum = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_txtChecksum.widthHint = 168;
		txtChecksum.setLayoutData(gd_txtChecksum);
		txtChecksum.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtChecksum.setEditable(false);

		Label lblCount = new Label(parent, SWT.NONE);
		lblCount.setAlignment(SWT.RIGHT);
		lblCount.setText(Messages.SavePointPart_count);

		txtCount = new Text(parent, SWT.BORDER);
		GridData gd_txtCount = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_txtCount.widthHint = 174;
		txtCount.setLayoutData(gd_txtCount);
		txtCount.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtCount.setEditable(false);

		Label lblFileNo = new Label(parent, SWT.NONE);
		lblFileNo.setAlignment(SWT.RIGHT);
		lblFileNo.setText(Messages.SavePointPart_fileNo);

		txtFileNo = new Text(parent, SWT.BORDER);
		GridData gd_txtFileNo = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_txtFileNo.widthHint = 172;
		txtFileNo.setLayoutData(gd_txtFileNo);
		txtFileNo.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtFileNo.setEditable(false);

		Label lblUpdateTime = new Label(parent, SWT.NONE);
		lblUpdateTime.setAlignment(SWT.RIGHT);
		lblUpdateTime.setText(Messages.SavePointPart_updateTime);

		txtUpdateTime = new Text(parent, SWT.BORDER);
		GridData gd_txtUpdateTime = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_txtUpdateTime.widthHint = 178;
		txtUpdateTime.setLayoutData(gd_txtUpdateTime);
		txtUpdateTime.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtUpdateTime.setEditable(false);

		log.info(this.getClass().getSimpleName() + " @PostConstruct method called."); //$NON-NLS-1$

//		initFormData();
	}

	@Inject
	@Optional
	void onClientBizMetaCreated(final @UIEventTopic(Pi2Events.CLIENT_BIZ_META_CREATED_SAVEPOINT) CmdEnum cmdEnum) {
		// do load data
		initFormData();
		eventBroker.post(Pi2Events.OPERATION, OpEnum.client_biz_meta_loaded.getDisplayText());
	}
	
	@Inject
	@Optional
	void onServerStart(final @UIEventTopic(Pi2Events.SERVER_STARTUP_SAVEPOINT) CmdEnum cmdEnum) {
		// TODO update UI state
		if (CmdEnum.startup.equals(cmdEnum)) {
			// TODO load initial data
			if (UIConst.RUN_AS_SERVER.equals(Pi2ContextUtil.getRunAs())) {
				initFormData();
				// update operation displayText
				// should be: s3_update_ui_savepoint_after_startup
				eventBroker.post(Pi2Events.OPERATION, OpEnum.s3_update_ui_after_startup.getDisplayText());
			}
			if (UIConst.RUN_AS_CLIENT.equals(Pi2ContextUtil.getRunAs())) {

				// case 1: runMode = NEW/RERUN the very first time run server, there is no
				// savepoint file, set notice
				if (RunModeEnum.NEW.equals(Pi2ContextUtil.getRunMode())) {
					// TODO set Message label info:
					// after client connected, server will generate/re-generate save-point file, and
					// will be loaded.
				}

				// case 2: RERUN RunModeEnum, load exist save-point info
				if (RunModeEnum.RERUN.equals(Pi2ContextUtil.getRunMode())) {
					initFormData();
					// update operation displayText
					// should be: s3_update_ui_savepoint_after_startup
					eventBroker.post(Pi2Events.OPERATION, OpEnum.s3_update_ui_after_startup.getDisplayText());
				}
			}
		}

	}
	
	@Inject
	@Optional
	void onClientStart(final @UIEventTopic(Pi2Events.CLIENT_STARTUP_SAVEPOINT) CmdEnum cmdEnum) {
		// case 1: runMode = NEW/RERUN the very first time run server, there is no
		// savepoint file, set notice
		if (RunModeEnum.NEW.equals(Pi2ContextUtil.getRunMode())) {
			// TODO set Message label info:
			// after client connected, server will generate/re-generate save-point file, and
			// will be loaded.
		}

		// case 2: RERUN RunModeEnum, load exist save-point info
		if (RunModeEnum.RERUN.equals(Pi2ContextUtil.getRunMode())) {
			initFormData();
			// update operation displayText
			// should be: s3_update_ui_savepoint_after_startup
			eventBroker.post(Pi2Events.OPERATION, OpEnum.s3_update_ui_after_startup.getDisplayText());
		}
	}
	
	@Inject
	@Optional
	void onServerShutdown(final @UIEventTopic(Pi2Events.SERVER_SHUTDOWN_SAVEPOINT) String message) {
		clearFormData();
	}
	
	@Inject
	@Optional
	void onClientShutdown(final @UIEventTopic(Pi2Events.CLIENT_SHUTDOWN_SAVEPOINT) String message) {
		clearFormData();
	}

	private void initFormData() {
		Savepoint sp = savepointService.load();
		if (sp == null) return;
		txtChecksum.setText(sp.getChecksum());
		txtCount.setText(String.valueOf(sp.getCount()));
		txtFileNo.setText(String.valueOf(sp.getFileNo()));
		txtUpdateTime.setText(longToDateStr(sp.getUpdateTime()));

	}
	
	private void clearFormData() {
		txtChecksum.setText(UIConst.EMPTY_STR);
		txtCount.setText(UIConst.EMPTY_STR);
		txtFileNo.setText(UIConst.EMPTY_STR);
		txtUpdateTime.setText(UIConst.EMPTY_STR);
	}

	private String longToDateStr(long time) {
		DateTime date = DateUtil.date(time);
		return DateUtil.formatDateTime(date);
	}
}
