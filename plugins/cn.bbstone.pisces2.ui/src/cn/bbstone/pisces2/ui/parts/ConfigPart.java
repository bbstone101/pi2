//package cn.bbstone.pisces2.ui.parts;
//
//import javax.annotation.PostConstruct;
//import javax.inject.Inject;
//
//import org.eclipse.swt.SWT;
//import org.eclipse.swt.widgets.Button;
//import org.eclipse.swt.widgets.Composite;
//import org.eclipse.swt.widgets.Label;
//import org.eclipse.swt.widgets.Text;
////import org.eclipse.wb.swt.SWTResourceManager;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import cn.bbstone.pisces2.model.Config;
//import cn.bbstone.pisces2.service.ConfigService;
//import cn.bbstone.pisces2.ui.base.SWTResourceManager;
//
//public class ConfigPart {
//	private static Logger log = LoggerFactory.getLogger(ConfigPart.class);
//	
//	@Inject
//	private ConfigService configService;
//	private Config config;
//	private Text txtPort;
//	private Text txtHost;
//	private Text txtFileFilter;
//	private Text txtRootPath;
//
//	public ConfigPart() {
////		System.out.println(this.getClass().getSimpleName() + " constructed");
//		log.info((this.getClass().getSimpleName() + " constructed"));
//	}
//
//	@PostConstruct
//	public void createControls(Composite parent) {
//		parent.setLayout(null);
//
//		Label lblPort = new Label(parent, SWT.NONE);
//		lblPort.setBounds(37, 59, 27, 21);
//		lblPort.setText("端口:");
//
//		txtPort = new Text(parent, SWT.BORDER);
//		txtPort.setBounds(70, 57, 68, 21);
//
//		Label lblServer = new Label(parent, SWT.NONE);
//		lblServer.setBounds(24, 86, 40, 21);
//		lblServer.setText("主机IP:");
//
//		txtHost = new Text(parent, SWT.BORDER);
//		txtHost.setBounds(70, 84, 140, 21);
//
//		Label lblFilter = new Label(parent, SWT.NONE);
//		lblFilter.setBounds(24, 113, 40, 21);
//		lblFilter.setText("过滤器:");
//
//		txtFileFilter = new Text(parent, SWT.BORDER);
//		txtFileFilter.setBounds(70, 111, 304, 21);
//
//		Label lblRootPath = new Label(parent, SWT.NONE);
//		lblRootPath.setBounds(24, 140, 40, 21);
//		lblRootPath.setText("根路径:");
//
//		txtRootPath = new Text(parent, SWT.BORDER);
//		txtRootPath.setBounds(70, 140, 304, 21);
//
//		Button btnReset = new Button(parent, SWT.NONE);
//		btnReset.setBounds(175, 167, 78, 21);
//		btnReset.setText("重置");
//
//		Button btnSave = new Button(parent, SWT.NONE);
//		btnSave.setBounds(296, 167, 78, 21);
//		btnSave.setText("保存");
//
//		Label lblNewLabel = new Label(parent, SWT.BORDER | SWT.WRAP | SWT.SHADOW_NONE);
//		lblNewLabel.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
//		lblNewLabel.setBounds(10, 10, 359, 41);
//		lblNewLabel.setText("通过设置以下配置项，确定服务器运行的IP，端口，扫描的文件根路径，过滤的文件等。");
////		System.out.println(this.getClass().getSimpleName() + " @PostConstruct method called.");
//		log.info(this.getClass().getSimpleName() + " @PostConstruct method called.");
//
////		System.out.println(" configService.load() called.");
//		log.info(" configService.load() called.");
//
//		initFormData();
//
//		// save config update to file after click config form 'save' button
//
//	}
//
//	private void initFormData() {
//		// load config from file
////		config = configService.load();
////		txtPort.setText(String.valueOf(config.getPort()));
////		txtHost.setText(config.getHost());
////		txtFileFilter.setText(config.getFilter());
////		txtRootPath.setText(config.getRootPath());
//	}
//
////	@Activate
////    public void activate() {
////        System.out.println("Activate called");
////        System.out.println("Loading Config(in ConfigPart) " + configService.load());
////    }
//}
