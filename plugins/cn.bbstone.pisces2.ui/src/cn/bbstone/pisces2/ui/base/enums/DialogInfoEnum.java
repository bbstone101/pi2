package cn.bbstone.pisces2.ui.base.enums;

public enum DialogInfoEnum {

	// startup check
	D10001("请确认", "服务器已启动，请先停止。"),
	D10002("请确认", "客户端已启动，请先停止。"),
	
	// shutdown check
	D10003("请确认", "是否还要停止服务器？"),
	D10004("请确认", "当前还有客户端连接未关闭，是否还要继续停止服务器？"),
	D10005("请确认", "是否还要停止客户端？"),
	
	D10006("请确认", "服务器还未启动。"),
	D10007("请确认", "客户端还未启动。"),
	
	D10008("请确认", "服务器正在运行，确定要关闭窗口并退出？"),
	D10009("请确认", "客户端正在运行，确定要关闭窗口并退出？"),
	
	D10010("请确认", "是否确认要关闭窗口并退出？"),
	
	Other("x", "y");
	
	private String title;
	private String message;
	
	DialogInfoEnum(String title, String message) {
		this.title = title;
		this.message = message;
	}

	public String getTitle() {
		return title;
	}

	public String getMessage() {
		return message;
	}
	
	
}
