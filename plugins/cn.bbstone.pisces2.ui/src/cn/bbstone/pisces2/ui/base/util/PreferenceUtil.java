package cn.bbstone.pisces2.ui.base.util;

import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.InstanceScope;

import cn.bbstone.pisces2.comm.Const;
import cn.bbstone.pisces2.config.ConfigFactory;
import cn.bbstone.pisces2.config.ConfigModel;
import cn.bbstone.pisces2.ui.preference.FieldInfoEnum;

/**
 * current util class belongs to bundle as the preferences node bundle
 * 
 * @author bbstone
 *
 */
public class PreferenceUtil {
	// the Preference Page bundle id
	public static final String NODE_ID = "cn.bbstone.pisces2.ui";

	// FrameworkUtil.getBundle(clazz).getSymbolicName()
	public static IEclipsePreferences getPreference() {
		// InstanceScope: only one workspace scope, after restart rcp, preference values will lost
		// ConfigurationScope, share with all workspace
		// DefaultScope 
		IEclipsePreferences preferences = InstanceScope.INSTANCE.getNode(NODE_ID);
		return preferences;
	}

	public static ConfigModel getConfigModel() {
		ConfigModel serverConfigModel = ConfigFactory.loadConfigs(Const.SERVER);
		ConfigModel clientConfigModel = ConfigFactory.loadConfigs(Const.CLIENT);
		
		IEclipsePreferences preferences = getPreference();
		ConfigModel configModel = new ConfigModel();
		configModel.setFilter(preferences.get(FieldInfoEnum.FLI_FILTER.getPreferenceName(), null));
		configModel.setServerPort(preferences.getInt(FieldInfoEnum.SERVER_PORT.getPreferenceName(), 0));
		configModel.setServerHost(preferences.get(FieldInfoEnum.SERVER_HOST.getPreferenceName(), null));
		configModel.setServerRoot(preferences.get(FieldInfoEnum.SERVER_ROOT.getPreferenceName(), null));
		configModel.setServerRootMac(serverConfigModel.getServerRootMac());
		configModel.setServerRootNix(serverConfigModel.getServerRootNix());
		configModel.setServerRootWin(serverConfigModel.getServerRootWin());
		// client only
		configModel.setClientRoot(preferences.get(FieldInfoEnum.CLIENT_ROOT.getPreferenceName(), null));
		configModel.setClientRootMac(clientConfigModel.getClientRootMac());
		configModel.setClientRootNix(clientConfigModel.getClientRootNix());
		configModel.setClientRootWin(clientConfigModel.getClientRootWin());
		
		configModel.setPostfix(preferences.get(FieldInfoEnum.CLIENT_FILE_TEMP_POSTFIX.getPreferenceName(), null));
		configModel.setConnNum(preferences.getInt(FieldInfoEnum.CLIENT_CONN_NUM.getPreferenceName(), 0));
		configModel
				.setOverwrite(preferences.getBoolean(FieldInfoEnum.CLIENT_FILE_OVERWRITE.getPreferenceName(), false));
		configModel.setRecvBuffer(preferences.get(FieldInfoEnum.CLIENT_RECV_BUFF.getPreferenceName(), null));

		return configModel;
	}

}
