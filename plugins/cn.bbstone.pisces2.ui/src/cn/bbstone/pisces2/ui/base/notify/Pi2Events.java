package cn.bbstone.pisces2.ui.base.notify;

public class Pi2Events {

	private static final String BASE_PKG = "cn/bbstone/pisces2/ui/";
	// ==================================== status bar ============
	/** Server/Client Status: online, offline, loading */
	public static final String PRESENCE = BASE_PKG + "statusbar/presence";

	/** server-client connected or not: connected, disconnected */
	public static final String CONNECTION = BASE_PKG + "statusbar/connection";

	/** User message update */
	public static final String OPERATION = BASE_PKG + "statusbar/operation";

	/** Messaging secure/unsecure */
	public static final String SECURITY = BASE_PKG + "statusbar/security";
	
	// ==================================== command ============
	/** startup, stop, config, exit */
	public static final String STARTUP = BASE_PKG + "command/startup";
	public static final String SHUTDOWN = BASE_PKG + "command/shutdown";
	
	public static final String SERVER_STARTUP_SAVEPOINT = BASE_PKG + "command/server/startup/savepoint";
	public static final String SERVER_STARTUP_FILELIST = BASE_PKG + "command/server/startup/filelist";
	public static final String SERVER_STARTUP_CONNINFO = BASE_PKG + "command/server/startup/conninfo";
	public static final String SERVER_STARTUP_HANDLER = BASE_PKG + "command/server/startup/handler";
	
	
	public static final String CLIENT_STARTUP_SAVEPOINT = BASE_PKG + "command/client/startup/savepoint";
	public static final String CLIENT_STARTUP_FILELIST = BASE_PKG + "command/client/startup/filelist";
	public static final String CLIENT_STARTUP_CONNINFO = BASE_PKG + "command/client/startup/conninfo";
	public static final String CLIENT_STARTUP_HANDLER = BASE_PKG + "command/client/startup/handler";
	public static final String CLIENT_CONNECTED_HANDLER = BASE_PKG + "command/client/connected/handler";
	
	public static final String CLIENT_BIZ_META_CREATED_SAVEPOINT = BASE_PKG + "command/client/biz/meta/created/notifySP";
	public static final String CLIENT_BIZ_META_CREATED_FILELIST = BASE_PKG + "command/client/biz/meta/created/notifyFL";
	
	
	public static final String CLIENT_SHUTDOWN_SAVEPOINT = BASE_PKG + "command/client/shutdown/savepoint";
	public static final String CLIENT_SHUTDOWN_FILELIST = BASE_PKG + "command/client/shutdown/filelist";
	public static final String CLIENT_SHUTDOWN_CONNINFO = BASE_PKG + "command/client/shutdown/conninfo";
	
	public static final String CLIENT_CONNECTED_CONNINFO = BASE_PKG + "command/client/connected/conninfo";
	public static final String CLIENT_DISCONNECTED_CONNINFO = BASE_PKG + "command/client/disconnected/conninfo";
	
	public static final String SERVER_DISCONNECTED_CONNINFO = BASE_PKG + "command/server/disconnected/conninfo";
	
	
	public static final String SERVER_SHUTDOWN_SAVEPOINT = BASE_PKG + "command/server/shutdown/savepoint";
	public static final String SERVER_SHUTDOWN_FILELIST = BASE_PKG + "command/server/shutdown/filelist";
	public static final String SERVER_SHUTDOWN_CONNINFO = BASE_PKG + "command/server/shutdown/conninfo";
	
	
	
//	public static final String CONFIG = "command/config";
//	public static final String EXIT = "command/exit";
	
	
}
