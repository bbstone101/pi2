package cn.bbstone.pisces2.services.impl;

import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.bbstone.pisces2.comm.Const;
import cn.bbstone.pisces2.config.ConfigFactory;
import cn.bbstone.pisces2.config.ConfigModel;
import cn.bbstone.pisces2.model.Config;
import cn.bbstone.pisces2.service.ConfigService;
import cn.bbstone.pisces2.util.CtxUtil;


@Component
public class ServerConfigServiceImpl implements ConfigService {
	
	private Logger log = LoggerFactory.getLogger(getClass());
	

	@Override
	public Config load() {
		log.info("[TODO] implement load data to Config Object from ~/.fli_server/fli.idx");
		Config config = new Config();
		
		// init configs, FileServer.startup() will load configs
		// if want to load configs before FileServer startup, need load configs first.
//        ConfigModel configModel = ConfigFactory.loadConfigs(Const.SERVER);
//        CtxUtil.setConfigModel(configModel);
        
		// TODO not load properties like following, just load from Activator.start() method
		config.setPort(CtxUtil.getConfigModel().getServerPort());
		config.setHost(CtxUtil.getConfigModel().getServerHost());
		config.setFilter(CtxUtil.getConfigModel().getFilter());
		config.setRootPath(CtxUtil.getConfigModel().getServerRoot());
		
		return config;
	}

	@Override
	public void store(Config config) {
		log.info("[TODO] implement store data to ~/.fli_server/fli.idx from Config Object");
		// TODO save config data to file
		
	}
	
	
	
	

}
