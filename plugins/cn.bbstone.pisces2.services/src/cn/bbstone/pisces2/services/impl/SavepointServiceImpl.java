package cn.bbstone.pisces2.services.impl;

import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.bbstone.pisces2.cmm.Pi2ContextUtil;
import cn.bbstone.pisces2.comm.fli.FliSavePoint;
import cn.bbstone.pisces2.model.Savepoint;
import cn.bbstone.pisces2.service.SavepointService;
import cn.bbstone.pisces2.util.FLIUtil;

@Component
public class SavepointServiceImpl implements SavepointService {

	private Logger log = LoggerFactory.getLogger(getClass());
//	

	@Override
	public Savepoint load() {

		FliSavePoint sp = Pi2ContextUtil.isRunAsServer() 
				? FLIUtil.readServerSavePoint()
				: FLIUtil.readClientSavePoint();
		// e.g. the very first time run, there is no fli.sp file in client/server
		if (sp == null) { 
			return null;
		}
		Savepoint savepoint = new Savepoint();
		savepoint.setChecksum(sp.getChecksum());
		savepoint.setCount(sp.getCount());
		savepoint.setFileNo(sp.getFileNo());
		savepoint.setUpdateTime(sp.getUpdateTime());

		log.info("save piont info loaded....");
		return savepoint;
	}

}
