package cn.bbstone.pisces2.cmm;

public interface UIConst {
	
	public static final String ICON_CHECKED = "/icons/part/checked.gif";
	public static final String ICON_UNCHECKED = "/icons/part/unchecked.gif";
	
	
	public static final String RUN_AS_SERVER = "server";
	public static final String RUN_AS_CLIENT = "client";
	
	public static final String EMPTY_STR = "";
}
