package cn.bbstone.pisces2.cmm;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import cn.bbstone.pisces2.config.ConfigModel;

public class Pi2ContextUtil {
	
	private static ConfigModel serverConfigModel = null;
	private static ConfigModel clientConfigModel = null;
	
	private static Map<String, Object> contextMap = new ConcurrentHashMap<>();
	// original value get via IEclipsePreference
	// cn.bbstone.pisces2.ui.base.util.PreferenceUtil.getPreference()
	
	// default runAs server
	private static String runAs = null; //UIConst.RUN_AS_SERVER;
	
	public static String getRunAs() {
//		if (contextMap.get(CtxKey.RUN_AS_KEY))
//		return String.valueOf(contextMap.get(CtxKey.RUN_AS_KEY));
//		if (!UIConst.RUN_AS_SERVER.equals(runAs) && !UIConst.RUN_AS_CLIENT.equals(runAs)) {
//			throw new IllegalStateException("runAs has unexpected value(should be server/client).");
//		}
		return runAs;
	}
	
	// 
	public static void setRunAs(String runAs) {
//		contextMap.put(CtxKey.RUN_AS_KEY, runAs);
		Pi2ContextUtil.runAs = runAs;
	}
	
	
	public static boolean isRunAsServer() {
		return UIConst.RUN_AS_SERVER.equals(runAs);
	}
	
	public static boolean isRunAsClient() {
		return UIConst.RUN_AS_CLIENT.equals(runAs);
	}
	
	public static RunModeEnum getRunMode() {
		return (RunModeEnum)contextMap.get(CtxKey.RUN_MODE_KEY);
	}
	
	public static void setRunMode(RunModeEnum runModeEnum) {
		contextMap.put(CtxKey.RUN_MODE_KEY, runModeEnum);
	}
	
	public static void setRunning(Boolean isRunning) {
		contextMap.put(CtxKey.IS_RUNNING_KEY, isRunning);
	}
	
	public static Boolean isRunning() {
		Object val = contextMap.get(CtxKey.IS_RUNNING_KEY);
		if (val != null) {
			return (Boolean) val;
		}
		return false;
	}
	
	public static void setClientConnected(Boolean connected) {
		contextMap.put(CtxKey.CLIENT_CONNECTED_KEY, connected);
	}
	
	public static boolean isClientConnected() {
		Object val = contextMap.get(CtxKey.CLIENT_CONNECTED_KEY);
		if (val != null) {
			return (Boolean) val;
		}
		return false;
	}
	
	
	public static void setServerConfigModel(ConfigModel srvConfigModel) {
		serverConfigModel = srvConfigModel;
	}
	
	public static ConfigModel getServerConfigModel() {
		return serverConfigModel;
	}
	
	public static void setClientConfigModel(ConfigModel cliConfigModel) {
		clientConfigModel = cliConfigModel;
	}
	
	public static ConfigModel getClientConfigModel() {
		return clientConfigModel;
	}
	
	
//	
//	public static void setPreference(IEclipsePreferences preferences) {
//		contextMap.put(CtxKey.PREFERENCE_KEY, preferences);
//	}
//	
//	public static IEclipsePreferences getPreference() {
//		Object val = contextMap.get(CtxKey.PREFERENCE_KEY);
//		if (val == null) return null;
//		return (IEclipsePreferences)val;
//	}
	
	
	
	
	
	
	public static final class CtxKey {
//		public static final String RUN_AS_KEY = "RUN_AS_KEY";
		public static final String RUN_MODE_KEY = "RUN_MODE_KEY";
		
		public static final String IS_RUNNING_KEY = "IS_RUNNING_KEY";
		
		public static final String CLIENT_CONNECTED_KEY = "CLIENT_CONNECTED_KEY";
		
		public static final String PREFERENCE_KEY = "PREFERENCE_KEY";
		
		
	}
	
	public static void reset() {
		contextMap.clear();
		
	}
	

}
