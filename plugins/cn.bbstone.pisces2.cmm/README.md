# cn.bbstone.pisces2.cmm
	 RCP 除了core和e4.ui之外各个bundle件共用的类放在这个插件项目里。
	 例如 包括 cn.bbstone.pisces2.xxx
	 	cn.bbstone.pisces2.model
	 	cn.bbstone.pisces2.service
	 	cn.bbstone.pisces2.ui
	 	
	 	但不包括：cn.bbstone.pisces2.core.xxx
	 	也不包括：cn.bbstone.e4.ui.xxx

## 介绍
	之所以需要这个common插件，是为了保证各个插件中相同的一些常量，方法等不用在多个插件里重复定义，也可以记录

和 插件 cn.bbstone.pisces2.core.cmm 不同的是，本cmm是用于RCP UI相关的插件项目，而core.cmm只要是尽量保证core（bussiness）的功能内聚，
不允许依赖与RCP UI相关的插件，否则，UI的插件依赖core.cmm，core.cmm又依赖ui的插件，就会形成循环依赖。要避免循环依赖。


## 其他说明
