/*******************************************************************************
 * Copyright (c) 2003, 2015 IBM Corporation and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     IBM - Initial API and implementation
 *******************************************************************************/
package cn.bbstone.e4.ui.log.job;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.jobs.IJobChangeEvent;
import org.eclipse.core.runtime.jobs.JobChangeAdapter;
import org.eclipse.e4.ui.di.UISynchronize;
import org.eclipse.ui.PlatformUI;


/**
 * 
 * NOTICE: this source file is copy from {@link org.eclipse.ui.progress.WorkbenchJob}, 
 * 	and adapted to match E4 application model.
 * 
 * 
 * WorkbenchJob is a type of job that implements a done listener and does the
 * shutdown checks before scheduling. This is used if a job is not meant to run
 * when the Workbench is shutdown.
 *
 * @since 3.0
 * @see org.eclipse.ui.progress.WorkbenchJob
 * 
 */
public abstract class E4WorkbenchJob extends E4UIJob {


	/**
	 * Add a new instance of the receiver with the supplied name.
	 *
	 * @param name String
	 */
	public E4WorkbenchJob(UISynchronize sync, String name) {
		super(sync, name);
		addDefaultJobChangeListener();
	}

	/**
	 * Add a job change listeners that handles a done event if the result was
	 * IStatus.OK.
	 *
	 */
	private void addDefaultJobChangeListener() {
		addJobChangeListener(new JobChangeAdapter() {

			@Override
			public void done(IJobChangeEvent event) {

				// Abort if it is not running
				if (!PlatformUI.isWorkbenchRunning()) {
					return;
				}

				if (event.getResult().getCode() == IStatus.OK) {
					performDone(event);
				}
			}
		});
	}

	/**
	 * Perform done with the supplied event. This will only occur if the returned
	 * status was OK. This is called only if the job is finished with an IStatus.OK
	 * result and the workbench is still running.
	 *
	 * @param event IJobChangeEvent
	 */
	public void performDone(IJobChangeEvent event) {
		// Do nothing by default.
	}

	@Override
	public boolean shouldSchedule() { // check workbenchRunning work for E3, E4 should remove it
		return super.shouldSchedule();// && PlatformUI.isWorkbenchRunning();
	}

	@Override
	public boolean shouldRun() { // check workbenchRunning work for E3, E4 should remove it
		return super.shouldRun();// && PlatformUI.isWorkbenchRunning();
	}

}
