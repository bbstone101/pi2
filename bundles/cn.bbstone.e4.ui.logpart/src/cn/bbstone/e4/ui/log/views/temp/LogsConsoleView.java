package cn.bbstone.e4.ui.log.views.temp;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.part.ViewPart;

// extends ConsoleView
public class LogsConsoleView  extends ViewPart {

	public static final String ID = "cn.bbstone.pisces2.ui.views.LogsConsoleView";
//	public LogsConsoleView() {
//		super();
//		
//		// 视图初始化时绑定唯一的MessageConsole 
//		ConsoleManager consoleManager = (ConsoleManager)LogsPrinter.getConsoleManager();
//		consoleManager.registerConsoleView(this);
//		consoleManager.showConsoleView(AppPrinter.getConsole());
//		consoleManager.addConsoleListener(this);
//	}
//	
//	@Override
//	public void createPartControl(Composite parent) {
//		super.createPartControl(parent);
//		IViewSite viewSite = getViewSite();
//		IActionBars actionBars = viewSite.getActionBars();
//		// 隐藏视图菜单
//		IMenuManager menuManager = actionBars.getMenuManager();
//		menuManager.setVisible(false);
//		// 隐藏工具条按钮
//		IToolBarManager toolBarManager = actionBars.getToolBarManager();
//		IContributionItem[] items = toolBarManager.getItems();
//		for(IContributionItem item: items) {
//			item.setVisible(false);
//		}
//		// 更新视图菜单
//		actionBars.updateActionBars();
//	}

	private Text text;

	public void createPartControl(Composite parent) {
		text = new Text(parent, SWT.READ_ONLY | SWT.MULTI);
		OutputStream out = new OutputStream() {
			@Override
			public void write(int b) throws IOException {
				if (text.isDisposed())
					return;
				text.append(String.valueOf((char) b));
			}
		};
		final PrintStream oldOut = System.out;
		System.setOut(new PrintStream(out));
		text.addDisposeListener(new DisposeListener() {
			public void widgetDisposed(DisposeEvent e) {
				System.setOut(oldOut);
			}
		});
	}

	public void setFocus() {
		text.setFocus();
	}

}
