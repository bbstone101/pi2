package cn.bbstone.e4.ui.log.logger;


/**
 * decouple dependency with LogsIOConsole
 * 
 * 
 * @author bbstone
 *
 */
public interface ILogConsole {
	
	/**
	 * create new outputStream for this console
	 * 
	 * NOTICE: one console can create one or more ouputStream
	 * 
	 * @return
	 */
	ILogOutputStream newOutputStream();
	
	
	void dispose();
	
}
