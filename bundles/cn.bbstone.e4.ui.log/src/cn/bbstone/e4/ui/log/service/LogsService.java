package cn.bbstone.e4.ui.log.service;

import java.io.OutputStream;

import org.eclipse.e4.core.services.events.IEventBroker;

import cn.bbstone.e4.ui.log.logger.ILogConsole;
import cn.bbstone.e4.ui.log.logger.ILogOutputStream;

public interface LogsService {

	/**
	 * Optional: for write log
	 * 
	 * @param logOutputStream
	 */
	void setLogOutputStream(ILogOutputStream logOutputStream);

	/**
	 * One Console can create one or many LogsIOConsoleOutputStream
	 * 
	 * @param logConsole
	 */
	void setLogConsole(ILogConsole logConsole);
	
	void setEventBroker(IEventBroker eventBroker);

	/**
	 * for log appender
	 * 
	 * @return
	 */
	OutputStream getLogOutputStream();

	void printLog(String log);

	void dispose();


	void handleClearConsole();

	void handleWordWrap();

	void handleLockConsole();

}
