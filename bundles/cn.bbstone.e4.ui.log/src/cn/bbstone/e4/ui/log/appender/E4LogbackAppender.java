package cn.bbstone.e4.ui.log.appender;

import java.io.OutputStream;

import ch.qos.logback.core.OutputStreamAppender;
import cn.bbstone.e4.ui.log.service.LogsService;

//@Component
public class E4LogbackAppender<E> extends OutputStreamAppender<E> {
//	private String testString;
	
//	@Inject
	private LogsService logsService;
	

	public void setLogsService(LogsService logsService) {
		this.logsService = logsService;
	}

	public E4LogbackAppender() {
		System.out.println("constructed, E4LogbackAppender,");
	}

	@Override
	protected void append(E eventObject) {
		super.append(eventObject);
		if (!isStarted()) {
            return;
        }
//		this.testString = "****<><><>world****<><><>";
//		logsService.printLog(testString);
//		System.out.println("method, E4LogbackAppender,appdend");
	}

	@Override
	public void start() {
		// must set outputStream
		OutputStream outputStream = logsService.getLogOutputStream();
		this.setOutputStream(outputStream);
		
		super.start();
//		System.out.println("method, E4LogbackAppender,startx");

	}

	@Override
	public void stop() {
		super.stop();
	}
	
	

}
