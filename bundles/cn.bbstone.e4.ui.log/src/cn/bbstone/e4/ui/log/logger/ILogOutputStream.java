package cn.bbstone.e4.ui.log.logger;

import java.io.IOException;

/**
 * decouple dependency with LogsIOConsoleInputStream
 * 
 * @author bbstone
 *
 */
public interface ILogOutputStream {

	public void write(byte[] b) throws IOException;
	
	public void write(String str) throws IOException;
	
	public void write(CharSequence chars) throws IOException;
	
	public void write(char[] buffer, int off, int len) throws IOException;
	
	public void write(char[] buffer) throws IOException;
	
	public void write(int b) throws IOException;
	
}
