package cn.bbstone.e4.ui.log.event;

public class LogPartEvents {

	private static final String BASE_PKG = "cn/bbstone/e4/ui/logpart/";
	
	
	public static final String CLEAR_CO = BASE_PKG + "command/clear_co";
	public static final String WORD_WRAP = BASE_PKG + "command/word_wrap";
	public static final String LOCK_CO = BASE_PKG + "command/lock_co";
	
	
	
}
